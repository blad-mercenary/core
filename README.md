# Core

[![pipeline status](https://gitlab.com/blad-mercenary/core/badges/dev/pipeline.svg)](https://gitlab.com/blad-mercenary/core/commits/dev) [![coverage report](https://gitlab.com/blad-mercenary/core/badges/dev/coverage.svg)](https://gitlab.com/blad-mercenary/core/commits/dev)

This project contain all of the neccessary objects for the game loop itself, but do not handle their implementation.

## Fight

This object contain everything needed for a fight. You will need to initialize it with a cast of charater (array of Character object, with character from both Team)

In a theorical implementation, this object would be used as such:

```js
// Character that will fight
const fighters = [
    Character.warrior(Team.player),
    Character.warrior(Team.enemy)
];

// Initialize the fight
const fight = new Fight(fighters);

// As long as the fight is not over
while(!fight.finished) {
    // Get the character playing this turn
    const activeFighter = fight.getActiveFighter();

    let selected;

    // On player turn
    if (activeFighter.team === Team.player) {
        // Get the possible selections
        const possibleSelections = getPossibleSelections(fight.getActiveFighter(), fight);

        // In whatever way, find whih Selection the player want to do
        selected = possibleSelections[getPlayerChoice()];
    }
    // On enemy turn
    if (activeFighter.team === Team.enemy) {
        selected = undefined;
    }

    // Play the turn
    fight.step(selected);
}

// Show who won
console.log(fight.winner);
```

You can find a very barebone example implementation in [the console project](https://gitlab.com/blad-mercenary/console), although it doesn't really enforce the game loop.
