import { Action, SelectDeadTargetIs, Target, TargetGroupIs } from '../lib/action';
import { Effects } from './effect';

export const Actions = {
	skip: (): Action =>
		new Action(
			'skip',
			'Skip',
			1,
			0,
			[],
			Target.self,
			TargetGroupIs.impossible,
			SelectDeadTargetIs.disallowed
		),
	hit: (): Action =>
		new Action(
			'hit',
			'Hit',
			3,
			0,
			[Effects.damage({ damage: 1, type: 'Physical' })],
			Target.enemies,
			TargetGroupIs.impossible,
			SelectDeadTargetIs.disallowed
		),
	'ice shard': (): Action =>
		new Action(
			'ice shard',
			'Ice Shard',
			3,
			1,
			[Effects.damage({ damage: 2, type: 'Special', element: 'Water' })],
			Target.enemies,
			TargetGroupIs.impossible,
			SelectDeadTargetIs.disallowed
		),
	'ice wave': (): Action =>
		new Action(
			'ice wave',
			'Ice Wave',
			5,
			4,
			[Effects.damage({ damage: 2, type: 'Special', element: 'Water' })],
			Target.enemies,
			TargetGroupIs.mandatory,
			SelectDeadTargetIs.disallowed
		),
	heal: (): Action =>
		new Action(
			'heal',
			'Heal',
			3,
			3,
			[],
			Target.allies,
			TargetGroupIs.impossible,
			SelectDeadTargetIs.disallowed
		),
	raise: (): Action =>
		new Action(
			'raise',
			'Raise',
			10,
			5,
			[Effects.resurrect({ heal: 3 })],
			Target.allies,
			TargetGroupIs.impossible,
			SelectDeadTargetIs.mandatory
		)
};

export type ActionId = keyof typeof Actions;
const validActionIds = Object.keys(Actions);

export function isActionId(value: string): value is ActionId {
	return validActionIds.includes(value);
}
