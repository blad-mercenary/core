import { Character, Team } from '../lib/character';
import { getRandomInt } from '../lib/utils';
import { ActionId } from './action';

export type CharacterType = 'goblin' | 'warrior' | 'mage' | 'healer'
export const CharacterType = ['goblin', 'warrior', 'mage', 'healer'];

export interface CharacterTypeData {
	type: string;
	name: string[];
	baseInit: number;
	maxHealth: number;
	maxMana: number;
	actions: ActionId[];
}

function getRandomNameFrom(names: string[]): string {
	return names[getRandomInt(0, names.length - 1)];
}

type CharacterBuilder = (team: Team, name?: string) => Character;

export const defaultCharacterNames: Record<CharacterType, string[]> = {
	goblin: [
		'Wril',
		'Tras',
		'Olk',
		'Akz',
		'Glibs',
		'Bleehbior',
		'Vigmioz',
		'Peegkoz',
		'Vreegniarm',
		'Vrozbalx'
	],
	warrior: [
		'Asbjørn Jan',
		'Christoffer Tore',
		'Truls Rúnar',
		'Fergie Lucas',
		'Rúnar Sune'
	],
	mage: [
		'Mághnus',
		'Séamus',
		'Shamus',
		'Ionatán',
		'Muirín'
	],
	healer: [
		'Ĉiela',
		'Karesinda',
		'Adorinda',
		'Gaja',
		'Nikolao'
	]
};

export const Characters: Record<CharacterType, CharacterBuilder> = {
	goblin: (team: Team, name?: string, level = 1): Character => {
		if (!name) {
			name = getRandomNameFrom(defaultCharacterNames.goblin);
		}
		return new Character(
			name,
			'goblin',
			team,
			'Wood',
			['skip', 'hit'],
			level,
			{ hp: 10, mp: 5 },
			{ hp: 1.1, mp: 1.05, atk: 1.15, def: 1.15, speAtk: 1.05, speDef: 1.05 }
		);
	},
	warrior: (team: Team, name?: string, level = 1): Character => {
		if (!name) {
			name = getRandomNameFrom(defaultCharacterNames.warrior);
		}
		return new Character(
			name,
			'warrior',
			team,
			'Fire',
			['skip', 'hit'],
			level,
			{ hp: 20, mp: 5, atk: 5, def: 3 },
			{ hp: 1.1, mp: 1.05, atk: 1.1, def: 1.1, speAtk: 1.05, speDef: 1.05 }
		);
	},
	mage: (team: Team, name?: string, level = 1): Character => {
		if (!name) {
			name = getRandomNameFrom(defaultCharacterNames.mage);
		}
		return new Character(
			name,
			'mage',
			team,
			'Water',
			['skip', 'ice shard', 'ice wave'],
			level,
			{ hp: 10, mp: 10, speAtk: 5, speDef: 3 },
			{ hp: 1.05, mp: 1.1, speAtk: 1.1, speDef: 1.05 }
		);
	},
	healer: (team: Team, name?: string, level = 1): Character => {
		if (!name) {
			name = getRandomNameFrom(defaultCharacterNames.healer);
		}
		return new Character(
			name,
			'healer',
			team,
			'Earth',
			['skip', 'hit', 'heal', 'raise'],
			level,
			{ hp: 10, mp: 10, speAtk: 3, speDef: 5 },
			{ hp: 1.05, mp: 1.1, speAtk: 1.05, speDef: 1.1 }
		);
	}
};

export function isCharacterType(value: string): value is CharacterType {
	return CharacterType.includes(value);
}
