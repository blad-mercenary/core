import { Character } from '../lib/character';
import { Effect } from '../lib/effects';
import { isElement, Element, elementalWeakness } from '../lib/elements';
import { Event, Events } from '../lib/event';

export interface FixedDamageDetails { damage: number }

function isFixedDamageDetails(details: EffectDetails): details is FixedDamageDetails {
	return !!details && 'damage' in details;
}

export type PhysicalDamageDetails = {
	type: 'Physical'
} & FixedDamageDetails;

export function isPhysicalDamageDetails(details: EffectDetails): details is PhysicalDamageDetails {
	return 'type' in details && details.type === 'Physical';
}

export type ElementalDamageDetails = {
	type: 'Special',
	element: Element
} & FixedDamageDetails;

export function isElementalDamageDetails(details: EffectDetails): details is ElementalDamageDetails {
	return 'type' in details && details.type === 'Special' && isElement(details.element);
}

export type DamageDetails = PhysicalDamageDetails | ElementalDamageDetails;

function isDamageDetails(details: EffectDetails): details is DamageDetails {
	return !!details
		&& 'damage' in details
		&& 'type' in details
		&& (
			details.type === 'Physical' ||
			(details.type === 'Special' && 'element' in details && isElement(details.element))
		);
}

export interface HealDetails { heal: number }

function isHealDetails(details: EffectDetails): details is HealDetails {
	return !!details && ('heal' in details);
}

export type EffectDetails = (
	FixedDamageDetails | DamageDetails | HealDetails
)
export type EffectImplementation = (target: Character, from: Character, details: EffectDetails) => Event;
export type EffectType = 'fixedDamage' | 'damage' | 'fixedHeal' | 'heal' | 'resurrect';

function formula(power: number, atk: number, def: number, weaknessFactor: number): number {
	return Math.floor(
		((power * atk / def) + 1) * weaknessFactor
	);
}

export const EffectImplementations: Record<EffectType, EffectImplementation> = {
	fixedDamage: (target: Character, from: Character, details: EffectDetails): Event => {
		if(!isFixedDamageDetails(details)) {
			throw new Error(`Wrong details given for damage ${JSON.stringify(details)}`);
		}
		target.health -= details.damage;
		if(target.health > target.maxHealth) {
			target.health = target.maxHealth;
		} else if(target.health < 0) {
			target.health = 0;
		}
		return Events.damage(from, target, details.damage);
	},
	damage: (target: Character, from: Character, details: EffectDetails): Event => {
		if(!isDamageDetails(details)) { throw new Error(`Wrong details given for damage ${JSON.stringify(details)}`); }
		let atk, def, weaknessFactor;

		if (details.type === 'Physical') {
			atk = from.stats.atk;
			def = target.stats.def;
			weaknessFactor = 1;
		} else if (details.type === 'Special') {
			atk = from.stats.speAtk;
			def = target.stats.speDef;
			if (elementalWeakness[target.element] === details.element) {
				weaknessFactor = 2;
			} else if (elementalWeakness[details.element] === target.element) {
				weaknessFactor = 0.5;
			} else {
				weaknessFactor = 1;
			}
		} else {
			throw new Error(`Invalid details ${JSON.stringify(details)}`);
		}

		const damage = formula(details.damage, atk, def, weaknessFactor);

		target.health -= damage;

		if(target.health > target.maxHealth) {
			target.health = target.maxHealth;
		} else if(target.health < 0) {
			target.health = 0;
		}
		return Events.damage(from, target, damage);
	},
	fixedHeal: (target: Character, from: Character, details: EffectDetails): Event => {
		if(!isHealDetails(details)) { throw new Error(`Wrong details given for heal ${JSON.stringify(details)}`); }
		target.health += details.heal;
		return Events.heal(from, target, details.heal);
	},
	heal: (target: Character, from: Character, details: EffectDetails): Event => {
		if(!isHealDetails(details)) { throw new Error(`Wrong details given for heal ${JSON.stringify(details)}`); }
		target.health += formula(details.heal, from.stats.speDef, 1, 1);
		return Events.heal(from, target, details.heal);
	},
	resurrect: (target: Character, from: Character, details: EffectDetails): Event => {
		if(!isHealDetails(details)) { throw new Error(`Wrong details given for heal ${JSON.stringify(details)}`); }
		EffectImplementations.heal(target, from, details);
		return Events.resurrect(from, target);
	}
};

export const Effects = {
	fixedDamage: (details: FixedDamageDetails): Effect =>
		new Effect('fixedDamage', details, EffectImplementations.fixedDamage),
	damage: (details: DamageDetails): Effect =>
		new Effect('damage', details, EffectImplementations.damage),
	fixedHeal: (details: HealDetails): Effect =>
		new Effect('fixedHeal', details, EffectImplementations.fixedHeal),
	heal: (details: HealDetails): Effect =>
		new Effect('heal', details, EffectImplementations.heal),
	resurrect: (details: HealDetails): Effect =>
		new Effect('resurrect', details, EffectImplementations.resurrect)
};
