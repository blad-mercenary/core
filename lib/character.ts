import { Action, Target } from './action';
import { ActionId, isActionId } from '../data/action';
import { v4 as uuid } from 'uuid';
import { Effect } from './effects';
import { Event, Events } from './event';
import { Stats, StatsSet, SavedStats, isSavedStats } from './stats';
import { isElement, Element } from './elements';

export enum Team {
	player = 'player',
	enemy = 'enemy'
}

export interface SavedCharacter {
	id: string;
	name: string;
	type: string;
	team: Team;
	element: Element;
	stats: SavedStats;
	init: number;
	health: number;
	mana: number;
	actions: ActionId[];
	lastTurn?: number;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function isSavedCharacter(value: any): value is SavedCharacter {
	return (
		typeof value === 'object'
		&& typeof value?.id === 'string'
		&& typeof value?.name === 'string'
		&& typeof value?.type === 'string'
		&& Object.values(Team).includes(value?.team)
		&& typeof value?.element === 'string' && isElement(value?.element)
		&& isSavedStats(value?.stats)
		&& typeof value?.init === 'number'
		&& typeof value?.health === 'number'
		&& typeof value?.mana === 'number'
		&& Array.isArray(value?.actions)
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		&& value?.actions.every((action: any) => typeof action === 'string' && isActionId(action))
		&& (typeof value?.lastTurn === 'undefined' || typeof value?.lastTurn === 'number')
	);
}

export class Character {
	public id: string;
	public name: string;
	public type: string;
	public team: Team;
	public element: Element;
	public init: number;
	public health: number;
	public mana: number;
	public actions: Action[];
	public lastTurn: number;
	public stats: Stats;

	public constructor(
		name: string,
		type: string,
		team: Team,
		element: Element,
		actions: ActionId[] = ['hit'],
		level = 1,
		stats: StatsSet,
		affinity: StatsSet,
		lastTurn: number = Number.POSITIVE_INFINITY
	) {
		this.id = uuid();
		this.name = name;
		this.type = type;
		this.team = team;
		this.element = element;
		this.stats = new Stats(stats, affinity, level);
		this.init = this.stats.init;
		this.health = this.stats.hp;
		this.mana = this.stats.mp;
		this.actions = actions.map((action) => Action.load(action));
		this.lastTurn = lastTurn;
	}

	public step(): void {
		this.lastTurn = 0;
		this.stats.step();
	}

	public incrementLastTurn(): void {
		this.lastTurn += 1;
	}

	public canUse(action: Action): boolean {
		return this.mana - action.mana >= 0;
	}

	public isAlive(): boolean {
		return this.health > 0;
	}

	public static copy(character: Character): Character {
		const stats = Stats.save(character.stats);
		const copy = new Character(
			character.name,
			character.type,
			character.team,
			character.element,
			character.actions.map((action) => action.id),
			stats.level,
			stats.initial,
			stats.affinity,
			character.lastTurn
		);
		copy.init = character.init;
		copy.health = character.health;
		copy.mana = character.mana;

		return copy;
	}

	public static save(character: Character): SavedCharacter {
		return {
			id: character.id,
			name: character.name,
			type: character.type,
			team: character.team,
			element: character.element,
			stats: Stats.save(character.stats),
			init: character.init,
			health: character.health,
			mana: character.mana,
			actions: character.actions.map((action): ActionId => Action.save(action)),
			lastTurn: (character.lastTurn === Number.POSITIVE_INFINITY) ? undefined : character.lastTurn,
		};
	}

	public static load(savedCharacter: SavedCharacter): Character {
		if (!isSavedCharacter(savedCharacter)) {
			throw new Error('Not a valid character');
		}
		const character = new Character(
			savedCharacter.name,
			savedCharacter.type,
			savedCharacter.team,
			savedCharacter.element,
			savedCharacter.actions,
			savedCharacter.stats.level,
			savedCharacter.stats.initial,
			savedCharacter.stats.affinity,
			savedCharacter.lastTurn
		);
		character.id = savedCharacter.id;
		character.init = savedCharacter.init;
		character.health = savedCharacter.health;
		character.mana = savedCharacter.mana;
		return character;
	}

	public apply(action: Action, target: Character): Event[] {
		const events = action.effects.map((effect: Effect): Event => {
			return effect.apply(target, this);
		});

		if (
			events.some((event) => event.type === 'damage')
			&& !target.isAlive()
		) {
			target.init = target.stats.init;
			target.lastTurn = Number.POSITIVE_INFINITY;
			events.push(Events.characterDied(target));
		}

		return events;
	}

	public canTarget(action: Action, target: Character): boolean {
		if (
			!(action.possibleTarget === Target.everyone)
			&& (
				(action.possibleTarget === Target.allies && target.team !== this.team)
				|| (action.possibleTarget === Target.enemies && target.team === this.team)
			)
		) {
			return false;
		}

		return action.validateTargetStatus(target.isAlive());
	}

	public get maxHealth(): number {
		return this.stats.hp;
	}

	public get maxMana(): number {
		return this.stats.mp;
	}
}
