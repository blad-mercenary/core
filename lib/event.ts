import { Action } from './action';
import { ActionId } from '../data/action';
import { Character, Team, SavedCharacter } from './character';
import { logger } from '@blad-mercenary/utils';
import { v4 as uuid } from 'uuid';
import { Player } from './player';
import { Fight } from './fight';

export interface ActionUsedEventDetails {
	from: string,
	action: ActionId;
	targetsIds: string[];
}

export interface CharacterDiedEventDetails {
	characterId: string;
	killedBy?: string;
}

export interface VictoryEventDetails {
	fight: string;
	winner: Team;
	fighters: SavedCharacter[];
}

export interface PlayerSaveEventDetails {
	player: string;
}

export interface PlayerLoadEventDetails {
	player: string;
}

export interface DamageEventDetails {
	from: string,
	target: string,
	damage: number
}

export interface HealEventDetails {
	from: string,
	target: string,
	heal: number
}

export interface ResurrectEventDetails {
	from: string,
	target: string
}

export interface NoActionEventDetails {
	from: string
}

type EventsDetails = (
	ActionUsedEventDetails
	| CharacterDiedEventDetails
	| VictoryEventDetails
	| PlayerSaveEventDetails
	| PlayerLoadEventDetails
	| DamageEventDetails
	| HealEventDetails
	| ResurrectEventDetails
	| NoActionEventDetails
)

export type EventType = 'actionUsed'
| 'characterDied' | 'victory' | 'playerSave' | 'playerLoad' | 'damage' | 'heal' | 'resurrect' | 'noAction';

export const EventType = [
	'actionUsed',
	'characterDied',
	'victory',
	'playerSave',
	'playerLoad',
	'damage',
	'heal',
	'resurrect',
	'noAction'
];

export interface SavedEvent {
	type: EventType;
	timestamp: string;
	details: EventsDetails;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function isSavedEvent(value: any): value is SavedEvent {
	return (
		typeof value === 'object'
		&& typeof value?.type === 'string'
		&& EventType.includes(value.type)
		&& typeof value?.timestamp === 'string'
		&& typeof value?.details === 'object'
	);
}

export class Event {
	public readonly type: EventType;
	public readonly timestamp: Date;
	public readonly details: EventsDetails;

	public constructor(type: EventType, details: EventsDetails, timestamp?: string) {
		this.type = type;
		if (timestamp) {
			this.timestamp = new Date(timestamp);
		} else {
			this.timestamp = new Date();
		}
		this.details = details;
	}

	public static save(event: Event): SavedEvent {
		return {
			type: event.type,
			timestamp: event.timestamp.toISOString(),
			details: event.details
		};
	}

	public static load(savedEvent: SavedEvent): Event {
		return new Event(savedEvent.type, savedEvent.details, savedEvent.timestamp);
	}
}

export const Events = {
	actionUsed: (
		from: Character, action: Action, targets: Character[], timestamp: Date = new Date()
	): Event =>
		new Event(
			'actionUsed',
			{ from: from.id, action: Action.save(action), targetsIds: targets.map((t): string => t.id) },
			timestamp.toISOString()
		),

	characterDied: (character: Character, killedBy?: Character, timestamp: Date = new Date()): Event =>
		new Event('characterDied', { characterId: character.id, killedBy: killedBy?.id }, timestamp.toISOString()),

	victory: (fight: Fight, timestamp: Date = new Date()): Event =>
		new Event(
			'victory',
			{ fight: fight.id, winner: fight.winner as Team, fighters: fight.fighters.map(Character.save) },
			timestamp.toISOString()
		),

	playerSave: (player: Player, timestamp: Date = new Date()): Event =>
		new Event('playerSave', { player: player.id }, timestamp.toISOString()),

	playerLoad: (player: Player, timestamp: Date = new Date()): Event =>
		new Event('playerLoad', { player: player.id }, timestamp.toISOString()),

	damage: (
		from: Character, target: Character, damage: number, timestamp: Date = new Date()
	): Event =>
		new Event('damage', { from: from.id, target: target.id, damage }, timestamp.toISOString()),

	heal: (
		from: Character, target: Character, heal: number, timestamp: Date = new Date()
	): Event =>
		new Event('heal', { from: from.id, target: target.id, heal }, timestamp.toISOString()),

	resurrect: (from: Character, target: Character, timestamp: Date = new Date()): Event =>
		new Event('resurrect', { from: from.id, target: target.id }, timestamp.toISOString()),

	noAction: (from: Character, timestamp: Date = new Date()): Event =>
		new Event('noAction', { from: from.id }, timestamp.toISOString())
};

export interface SavedEventLogger {
	id: string;
	logs: SavedEvent[];
}


// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function isSavedEventLogger(value: any): value is SavedEventLogger {
	return (
		typeof value === 'object'
		&& typeof value?.id === 'string'
		&& Array.isArray(value?.logs)
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		&& value.logs.every((el: any) => isSavedEvent(el))
	);
}

function printEvent(event: Event, fighters: Character[]): string {
	function name(id: string): string {
		const fighter = fighters.find((fighter) => fighter.id === id);
		if (!fighter) {
			throw new Error(`Cannot find id ${id}`);
		}
		return `${fighter.name} (${fighter.id}) (${fighter.team})`;
	}

	if (event.type === 'actionUsed') {
		const details = event.details as ActionUsedEventDetails;
		const from = name(details.from);
		const action = Action.load(details.action);
		const targets = details.targetsIds.map(name);

		return `${from} used ${action.name} on ${targets.join(', ')}`;
	}

	if (event.type === 'characterDied') {
		const details = event.details as CharacterDiedEventDetails;
		const character = details.characterId;

		return `${character} died`;
	}

	if (event.type === 'victory') {
		const details = event.details as VictoryEventDetails;

		return `${details.winner} won`;
	}

	if (event.type === 'playerSave') {
		return 'Player save';
	}

	if (event.type === 'playerLoad') {
		return 'Player load';
	}

	if (event.type === 'damage') {
		const details = event.details as DamageEventDetails;
		const from = name(details.from);
		const target = name(details.target);

		return `${from} inflicted ${details.damage} damage to ${target}`;
	}

	if (event.type === 'heal') {
		const details = event.details as HealEventDetails;
		const from = name(details.from);
		const target = name(details.target);

		return `${from} healed ${target} of ${details.heal} damage`;
	}

	if (event.type === 'resurrect') {
		const details = event.details as ResurrectEventDetails;
		const from = name(details.from);
		const target = name(details.target);

		return `${from} resurected ${target}`;
	}

	if (event.type === 'noAction') {
		const details = event.details as NoActionEventDetails;
		const from = name(details.from);

		return `${from} did nothing`;
	}

	throw new Error(`Unknown error type ${event.type}`);
}

export class EventLogger {
	public id: string;
	private logs: Event[];
	private lastRead: number;

	public constructor(id: string = uuid(), logs?: Event[]) {
		this.id = id;
		if (logs) {
			this.logs = logs;
			this.lastRead = logs.length;
		} else {
			this.logs = [];
			this.lastRead = 0;
		}
	}

	public log(event: Event | Event[]): void {
		logger.info('event', event);
		if (Array.isArray(event)) {
			this.logs.push(...event);
		} else {
			this.logs.push(event);
		}
	}

	public getEvents(since?: Date): Event[] {
		if (since) {
			return this.logs.filter((event) => event.timestamp > since);
		} else {
			return this.logs;
		}
	}

	public getLatest(): Event[] {
		const latest = this.logs.slice(this.lastRead);
		this.lastRead = this.logs.length;
		return latest;
	}

	public static save(eventLogger: EventLogger): SavedEventLogger {
		return {
			id: eventLogger.id,
			logs: eventLogger.logs.map(Event.save)
		};
	}

	public static load(sel: SavedEventLogger): EventLogger {
		return new EventLogger(sel.id, sel.logs.map(Event.load));
	}

	public static print(events: Event[], fighters: Character[]): string[] {
		return events.map((event) => `${event.timestamp} - ${printEvent(event, fighters)}`);
	}
}

export const defaultEventLogger = new EventLogger('default');
