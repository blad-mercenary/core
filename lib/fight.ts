import { Team, Character, SavedCharacter, isSavedCharacter } from './character';
import { Selection } from './selection';
import { Events, EventLogger, SavedEventLogger } from './event';
import { v4 as uuid } from 'uuid';
import { getRandomInt } from './utils';
import { Actions } from '../data';

export interface SavedFight {
	fighters: SavedCharacter[];
	finished: boolean;
	winner?: Team;
	logger: SavedEventLogger;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function isSavedFight(value: any): value is SavedFight {
	return (
		typeof value === 'object'
		&& Object.prototype.hasOwnProperty.call(value, 'fighters') && Array.isArray(value.fighters)
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		&& (value.fighters as any[]).every((f): boolean => isSavedCharacter(f))
		&& Object.prototype.hasOwnProperty.call(value, 'finished')
		&& (
			value.winner === undefined
			|| Object.keys(Team).includes(value.winner)
		)
	);
}

export class Fight {
	/** Id of the fight. */
	public readonly id: string;
	/** List of all fighters. */
	public fighters: Character[];
	/** If the fight have ended. */
	public finished: boolean;
	/** When finished, which Team have won. */
	public winner?: Team;
	/** Logger of all events to be shown to the player. */
	public readonly eventLogger: EventLogger;

	public constructor(fighters: Character[], eventLogger?: EventLogger) {
		if (fighters.length === 0) {
			throw new Error('Invalid fight, no fighters given');
		}
		if(!fighters.some((c: Character): boolean => c.team === Team.player)) {
			throw new Error('Invalid fight, no fighters controlled by the player');
		}
		if(!fighters.some((c: Character): boolean => c.team !== Team.player)) {
			throw new Error('Invalid fight, no fighters not controlled by the player');
		}

		this.id = uuid();
		this.fighters = fighters;
		this.finished = false;

		if(eventLogger) {
			this.eventLogger = eventLogger;
		} else {
			this.eventLogger = new EventLogger();
		}
	}

	/**
	 * @param fight
	 * @returns JSON serialisable version of the fight.
	 */
	public static save(fight: Fight): SavedFight {
		return {
			fighters: fight.fighters.map(Character.save),
			finished: fight.finished,
			winner: fight.winner,
			logger: EventLogger.save(fight.eventLogger)
		};
	}

	public static load(savedFight: SavedFight): Fight {
		const fight = new Fight(savedFight.fighters.map(Character.load), EventLogger.load(savedFight.logger));
		fight.finished = savedFight.finished;
		fight.winner = savedFight.winner;
		return fight;
	}

	/**
	 * @returns The character that will act on the next step.
	 */
	public getActiveFighter(): Character {
		return this.getNextActiveFighters()[0];
	}

	/**
	 * @returns All characters, ordered by which one should play next.
	 */
	public getNextActiveFighters(): Character[] {
		if(this.finished) {
			throw new Error('Fight already finished');
		}

		const activeFighters = this.fighters.filter((fighter): boolean => fighter.isAlive());

		if (activeFighters.length === 0) {
			throw new Error('No valid fighters');
		}

		activeFighters.sort((a: Character, b: Character): number => {
			if (a.init > b.init) {
				return 1;
			}
			if (b.init > a.init) {
				return -1;
			}
			if (a.lastTurn < b.lastTurn) {
				return 1;
			}
			if (b.lastTurn > a.lastTurn) {
				return -1;
			}
			if (a.id > b.id) {
				return 1;
			}
			if (b.id > a.id) {
				return -1;
			}
			return 0;
		});

		const activeFighter = activeFighters[0];

		if (activeFighter.init > 0) {
			const i = activeFighter.init;
			this.fighters = this.fighters.map((fighter): Character => {
				fighter.init = fighter.init - i;
				return fighter;
			});
		}

		return activeFighters;
	}

	/**
	 * @param activeFighter - Active character
	 * @param selection - Either the selection, or its index in the possible selection list.
	 * @returns Valid selection, or undefined if there is no possible actions.
	 */
	public validateSelection(activeFighter: Character, selection?: number|Selection): Selection|undefined {
		const possibleActions = Selection.getPossibleSelections(activeFighter, this);

		if (
			typeof selection === 'object'
		) {
			const validated = Selection.validate(selection, activeFighter, this);
			if (Selection.isSelection(selection) && validated) {
				return selection;
			}
			throw new Error(`Invalid selection ${JSON.stringify(selection)}`);
		}

		if (typeof selection === 'number') {
			if (possibleActions[selection]) {
				return possibleActions[selection];
			}
			throw new Error(`Invalid selection index ${selection}`);
		}

		if (selection === undefined) {
			if (possibleActions.length === 0) {
				return;
			}
			const random = getRandomInt(0, possibleActions.length - 1);
			return possibleActions[random];
		}

		// Unreachable
		throw new Error(`Invalid selection ${selection}`);
	}

	/**
	 * Advance one turn, using the selected action.
	 * If selection is undefined and there are no possible actions, skip the turn.
	 * If selection is undefined and there are possible actions, select a random one.
	 * @param selection - Selection object, selection index or undefined for auto selection or no actions.
	 */
	public step(selection?: number|Selection): void {
		const activeFighter = this.getActiveFighter();

		this.fighters.forEach((fighter) => {
			if (fighter.isAlive()) {
				fighter.incrementLastTurn();
			}
		});
		activeFighter.step();

		const selectedAction =
			this.validateSelection(activeFighter, selection)
			|| { action: Actions.skip(), targets: [activeFighter] };

		const { action, targets } = selectedAction;

		this.eventLogger.log(Events.actionUsed(activeFighter, action, targets));

		targets.forEach((target: Character): void => {
			const events = activeFighter.apply(action, target);
			this.eventLogger.log(events);
		});

		activeFighter.init += action.time;
		activeFighter.mana -= action.mana;

		if (!this.hasFighterAliveIn(Team.player)) {
			this.victoryBy(Team.enemy);
		} else if (!this.hasFighterAliveIn(Team.enemy)) {
			this.victoryBy(Team.player);
		}
	}

	public hasFighterAliveIn(team: Team): boolean {
		return this.fighters.filter(
			(c: Character): boolean => c.team === team
		).some(
			(c: Character): boolean => c.isAlive()
		);
	}

	public getTeam(team: Team): Character[] {
		return this.fighters.filter((fighter) => fighter.team === team);
	}

	/**
	 * Declare victory for the given team.
	 * @param team
	 */
	public victoryBy(team: Team): void {
		if (this.finished) {
			throw new Error('Fight already finished');
		}
		this.finished = true;
		this.winner = team;
		this.eventLogger.log(Events.victory(this));
	}
}
