export interface FullStatsSet {
	hp: number;
	mp: number;
	atk: number;
	def: number;
	speAtk: number;
	speDef: number;
	init: number;
}

export function isFullStatsSet(saved: any): saved is FullStatsSet {
	return typeof saved === 'object'
		&& typeof saved?.hp === 'number'
		&& typeof saved?.mp === 'number'
		&& typeof saved?.atk === 'number'
		&& typeof saved?.def === 'number'
		&& typeof saved?.speAtk === 'number'
		&& typeof saved?.speDef === 'number'
		&& typeof saved?.init === 'number';
}

export interface StatsSet {
	hp?: number;
	mp?: number;
	atk?: number;
	def?: number;
	speAtk?: number;
	speDef?: number;
	init?: number;
}

function getStatSet(value: number): FullStatsSet {
	return { hp: value, mp: value, atk: value, def: value, speAtk: value, speDef: value, init: value };
}

export function toFullStatsSet(partial: StatsSet, defaultStat = 1): FullStatsSet {
	return {
		...getStatSet(defaultStat),
		...partial
	};
}

export const statTypes: StatType[] = ['hp', 'mp', 'atk', 'def', 'speAtk', 'speDef', 'init'];

export type StatType = keyof FullStatsSet;

export interface SavedModifier {
	value: FullStatsSet;
	expires?: number;
}

export function isSavedModifier(saved: any): saved is SavedModifier {
	return typeof saved === 'object'
		&& isFullStatsSet(saved?.value)
		&& (saved?.expires === undefined || typeof saved.expires === 'number');
}

export interface SavedStats {
	initial: FullStatsSet;
	affinity: FullStatsSet;
	modifiers: SavedModifier[];
	level: number;
}

export function isSavedStats(saved: any): saved is SavedStats {
	return typeof saved === 'object'
		&& isFullStatsSet(saved?.initial)
		&& isFullStatsSet(saved?.affinity)
		&& Array.isArray(saved?.modifiers) && saved.modifiers.every((element: any) => isSavedModifier(element))
		&& typeof saved?.level === 'number';
}

export class StatModifier {
	public value: FullStatsSet;
	public expires?: number;

	constructor(value: StatsSet, expires?: number) {
		this.value = {
			...value,
			...getStatSet(0)
		};
		this.expires = expires;
	}
}

export class Stats {
	private initial: FullStatsSet;
	private affinity: FullStatsSet;
	private _modifiers: StatModifier[];
	private level: number;
	private statsCache: FullStatsSet;

	/**
	 * @param initial - Initial stats, represent the "real" stats of the character at level 1
	 * @param affinity - Growth factor for each stats
	 * @param level - Level of the character, used to compute the current stats
	 * @param modifiers - List of all stats modifiers. Modifiers are used as factors of the stat
	 */
	constructor(init: StatsSet, affinity: StatsSet, level: number, modifiers: StatModifier[] = []) {
		this.initial = toFullStatsSet(init);
		this.affinity = toFullStatsSet(affinity);
		this._modifiers = modifiers;
		this.level = level;
		this.statsCache = getStatSet(1);
		this.compute(level);
	}

	protected save(): SavedStats {
		return {
			initial: this.initial,
			affinity: this.affinity,
			level: this.level,
			modifiers: this._modifiers.map((modifier) => ({
				value: modifier.value,
				expires: modifier.expires
			}))
		};
	}

	static save(stats: Stats): SavedStats {
		return stats.save();
	}

	static load(saved: SavedStats): Stats {
		return new Stats(saved.initial, saved.affinity, saved.level, saved.modifiers);
	}

	/**
	 * Compute the actual stats
	 * @param level
	 */
	compute(level?: number): void {
		if (level) {
			this.level = level;
		}

		const modifiers = this._modifiers.reduce((acc: FullStatsSet, cur: StatModifier) => {
			const result = getStatSet(0);
			statTypes.forEach((stat: StatType): void => {
				result[stat] = acc[stat] * cur.value[stat];
			});
			return result;
		}, getStatSet(1));

		statTypes.forEach((stat: StatType): void => {
			this.statsCache[stat] = Math.floor(this.initial[stat] * this.affinity[stat] * this.level * modifiers[stat]);
		});
	}

	public get modifiers(): StatModifier[] {
		return this._modifiers;
	}

	public set modifiers(modifiers: StatModifier[]) {
		this._modifiers = modifiers;
		this.compute();
	}

	/**
	 * Hook to be used on each turn, will check if any modifier expired, and recompute if needed
	 */
	public step(): void {
		const modifierCount = this._modifiers.length;

		this._modifiers = this._modifiers.map((modifier) => {
			if(modifier.expires !== undefined) {
				modifier.expires -= 1;
			}
			return modifier;
		}).filter((modifier) => {
			return modifier.expires === undefined || modifier.expires >= 0;
		});

		if (this._modifiers.length !== modifierCount) {
			this.compute(this.level);
		}
	}

	/**
	 * General getter for the stats
	 * @param stat - Stat to fetch
	 * @returns The stat value
	 */
	get(stat: StatType): number {
		return this.statsCache[stat];
	}

	public get hp(): number {
		return this.get('hp');
	}

	public get mp(): number {
		return this.get('mp');
	}

	public get atk(): number {
		return this.get('atk');
	}

	public get def(): number {
		return this.get('def');
	}

	public get speAtk(): number {
		return this.get('speAtk');
	}

	public get speDef(): number {
		return this.get('speDef');
	}

	public get init(): number {
		return this.get('init');
	}
}