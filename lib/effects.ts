import { EffectDetails, EffectType, EffectImplementation } from '../data/effect';
import { Character } from './character';
import { Event } from './event';

export class Effect {
	public type: EffectType;
	public details: EffectDetails;
	private _apply: EffectImplementation;

	public constructor(type: EffectType, details: EffectDetails, apply: EffectImplementation) {
		this.type = type;
		this.details = details;
		this._apply = apply;
	}

	public apply(target: Character, from: Character): Event {
		return this._apply(target, from, this.details);
	}
}
