import { ActionId, Actions, isActionId } from '../data/action';
import { Effect } from './effects';

export enum Target {
	everyone,
	allies,
	enemies,
	self
}

export enum SelectDeadTargetIs {
	allowed,
	disallowed,
	mandatory
}

export enum TargetGroupIs {
	impossible,
	// possible,
	mandatory
}

export class Action {
	public id: ActionId;
	public name: string;
	public time: number;
	public mana: number;
	public effects: Effect[];
	public possibleTarget: Target;
	public groupAction: TargetGroupIs;
	public deathSelection: SelectDeadTargetIs;

	public constructor(
		id: ActionId,
		name: string,
		time: number,
		mana: number,
		effects: Effect[],
		possibleTarget: Target,
		groupAction: TargetGroupIs,
		deathSelection: SelectDeadTargetIs
	) {
		this.id = id;
		this.name = name;
		this.time = time;
		this.mana = mana;
		this.effects = effects;
		this.possibleTarget = possibleTarget;
		this.groupAction = groupAction;
		this.deathSelection = deathSelection;
	}

	public static save(action: Action): ActionId {
		if (!isActionId(action.id)) {
			throw new Error(`Invalid action ${action}`);
		}
		return action.id;
	}

	public static load(actionId: string): Action {
		if (!isActionId(actionId)) {
			throw new Error(`Trying to load invalid Action data ${JSON.stringify(actionId)}`);
		}

		// eslint-disable-next-line @typescript-eslint/no-use-before-define
		return Actions[actionId as ActionId]();
	}

	public validateTargetStatus(isTargetAlive: boolean): boolean {
		if (this.deathSelection === SelectDeadTargetIs.allowed) {
			return true;
		} else if (this.deathSelection === SelectDeadTargetIs.disallowed) {
			return isTargetAlive;
		} else if (this.deathSelection === SelectDeadTargetIs.mandatory) {
			return !isTargetAlive;
		} else {
			// Unreachable
			throw new Error(`Invalid deathSelection ${this.deathSelection}`);
		}
	}
}
