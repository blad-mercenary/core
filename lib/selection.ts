import { Character } from './character';
import { Action, SelectDeadTargetIs, Target, TargetGroupIs } from './action';
import { Fight } from './fight';

export interface Selection {
	action: Action;
	targets: Character[];
}

function isSelection(obj: any): obj is Selection {
	return (
		typeof obj === 'object'
		&& obj.action && obj.action instanceof Action
		&& obj.targets && Array.isArray(obj.targets) && obj.targets.every((target: any) => target instanceof Character)
	);
}

function getSelectionsForAction(action: Action, character: Character, fight: Fight): Selection[] {
	let potentialTargets: Character[];

	if (action.possibleTarget === Target.self) {
		potentialTargets = [character];
	} else if (action.deathSelection === SelectDeadTargetIs.allowed) {
		potentialTargets = fight.fighters;
	} else if (action.deathSelection === SelectDeadTargetIs.disallowed) {
		potentialTargets = fight.fighters.filter((f): boolean => f.isAlive());
	} else if (action.deathSelection === SelectDeadTargetIs.mandatory) {
		potentialTargets = fight.fighters.filter((f): boolean => !f.isAlive());
	} else {
		// Cannot be reached
		throw new Error(`Invalid deathSelection ${action.deathSelection}`);
	}

	return potentialTargets
		.filter((fighter: Character): boolean => character.canTarget(action, fighter))
		.map((fighter: Character): Selection => ({ action, targets: [fighter] }));
}

function getSelectionsForGroupAction(action: Action, character: Character, fight: Fight): Selection[] {
	const allies = fight.fighters.filter((fighter: Character): boolean => fighter.team === character.team);
	const enemies = fight.fighters.filter((fighter: Character): boolean => fighter.team !== character.team);

	function select(group: Character[]): Character[] {
		return group.filter(
			(c): boolean => character.canTarget(action, c)
		);
	}

	const selections = [];

	if (action.possibleTarget === Target.self) {
		selections.push({ action, targets: [character] });
	} else if (action.possibleTarget === Target.everyone) {
		selections.push({ action, targets: select(allies) });
		selections.push({ action, targets: select(enemies) });
	} else if (action.possibleTarget === Target.allies) {
		selections.push({ action, targets: select(allies) });
	} else if (action.possibleTarget === Target.enemies) {
		selections.push({ action, targets: select(enemies) });
	}

	return selections.filter((s): boolean => s.targets.length > 0);
}

function getPossibleSelections(character: Character, fight: Fight): Selection[] {
	const possibleActions = character.actions.filter((action: Action): boolean => character.canUse(action));

	return possibleActions.map((action: Action): Selection[] => {
		if (action.groupAction === TargetGroupIs.impossible) {
			return getSelectionsForAction(action, character, fight);
		} else {
			return getSelectionsForGroupAction(action, character, fight);
		}
	}).reduce((acc: Selection[], cur: Selection[]): Selection[] => acc.concat(cur), []);
}

function validate(selection: Selection, character: Character, fight: Fight): boolean {
	const possibleActions = getPossibleSelections(character, fight);
	const selectionTargets = selection.targets.map((character) => character.id);

	return possibleActions.some((possible: Selection) => {
		const possibleTargets = possible.targets.map((character) => character.id);

		return selection.action === possible.action
			&& selectionTargets.every((character) => possibleTargets.includes(character))
			&& possibleTargets.every((character) => selectionTargets.includes(character));
	});
}

export const Selection = {
	isSelection,
	getSelectionsForAction,
	getSelectionsForGroupAction,
	getPossibleSelections,
	validate
};
