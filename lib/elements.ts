export type Element = 'Wood' | 'Fire' | 'Earth' | 'Metal' | 'Water';
export const allElements = ['Wood', 'Fire', 'Earth', 'Metal', 'Water'];

export const elementalWeakness: { [key in Element]: Element } = {
	Wood: 'Water',
	Fire: 'Wood',
	Earth: 'Fire',
	Metal: 'Earth',
	Water: 'Metal'
};

export function isElement(string: string): string is Element {
	return allElements.includes(string);
}
