import { v4 as uuid } from 'uuid';
import { Character, Team, SavedCharacter, isSavedCharacter } from './character';
import { EventLogger, SavedEventLogger, Events } from './event';
import { Characters } from '../data/character';
import { isSavedEventLogger } from '.';

export interface SavedPlayer {
	id: string;
	name: string;
	gold: number;
	team: SavedCharacter[];
	eventLogger: SavedEventLogger;
	version: number;
}

export class Player {
	private _id: string;
	public name: string;
	public gold: number;
	public team: Character[];
	public eventLogger: EventLogger;
	public version: number;

	public constructor(
		id: string = uuid(),
		name = 'Jack',
		gold = 1000,
		team: Character[] = [Characters.goblin(Team.player)],
		eventLogger: EventLogger = new EventLogger(),
		version = 0
	) {
		this._id = id;
		this.name = name;
		this.gold = gold;
		this.team = team;
		this.eventLogger = eventLogger;
		this.version = version;
	}

	public static save(player: Player, noLog?: boolean): SavedPlayer {
		if (!noLog) {
			player.eventLogger.log(Events.playerSave(player));
		}
		return {
			id: player._id,
			name: player.name,
			gold: player.gold,
			team: player.team.map(Character.save),
			eventLogger: EventLogger.save(player.eventLogger),
			version: player.version
		};
	}

	public static load(sp: SavedPlayer, version = 0, noLog?: boolean): Player {
		const player = new Player(
			sp.id,
			sp.name,
			sp.gold,
			sp.team.map(Character.load),
			EventLogger.load(sp.eventLogger),
			version
		);
		if (!noLog) {
			player.eventLogger.log(Events.playerLoad(player));
		}
		return player;
	}

	public get id(): string {
		return this._id;
	}

	public get status(): SavedPlayer {
		return Player.save(this, true);
	}
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function isSavedPlayer(value: any): value is SavedPlayer {
	return (
		typeof value === 'object'
		&& typeof value?.id === 'string'
		&& typeof value?.name === 'string'
		&& typeof value?.gold === 'number'
		&& Array.isArray(value?.team)
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		&& value.team.every((el: any) => isSavedCharacter(el))
		&& typeof isSavedEventLogger(value?.eventLogger)
		&& typeof value?.version === 'number'
	);
}
