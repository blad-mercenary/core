import { describe, it } from 'mocha';
import { expect } from 'chai';
import { Effects, EffectImplementations } from '../../data/effect';
import * as sinon from 'sinon';
import { Team } from '../../lib/character';
import { Characters } from '../../data/character';
import { Action, Target, TargetGroupIs, SelectDeadTargetIs } from '../../lib/action';

describe('Effect', (): void => {
	describe('apply', (): void => {
		it('apply the effect to the target character', (): void => {
			const damage = sinon.spy(EffectImplementations, 'fixedDamage');
			const details = { damage: 1 };
			const action = new Action(
				'hit',
				'test',
				1,
				1,
				[Effects.fixedDamage(details)],
				Target.enemies,
				TargetGroupIs.mandatory,
				SelectDeadTargetIs.disallowed
			);
			const from = Characters.goblin(Team.player, 'player');
			const target = Characters.goblin(Team.enemy);

			from.apply(action, target);

			expect(damage.called).to.be.true;
			expect(damage.calledWith(target, from, details)).to.be.true;
		});
	});
});
