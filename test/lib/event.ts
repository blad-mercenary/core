import { describe, it } from 'mocha';
import { expect } from 'chai';
import { EventLogger, Events, Event, EventType } from '../../lib/event';
import { Actions } from '../../data/action';
import { Team } from '../../lib/character';
import { Characters } from '../../data/character';
import { Fight } from '../../lib/fight';

describe('Event', (): void => {
	function getTestEvent(date?: Date): Event {
		return Events.actionUsed(Characters.goblin(Team.player), Actions.hit(), [Characters.goblin(Team.enemy)], date);
	}

	describe('EventLogger', (): void => {
		const eventLogger = new EventLogger('test');
		const testEvent = getTestEvent();
		eventLogger.log(testEvent);

		describe('static', (): void => {
			describe('save', (): void => {
				it('returns a serializable object', (): void => {
					const savedEventLogger = EventLogger.save(eventLogger);

					expect(savedEventLogger).to.be.an('object');
					expect((): string => { return JSON.stringify(savedEventLogger); }).to.not.throw;
				});
			});

			describe('load', (): void => {
				it('returns a EventLogger when called with a saved event logger', (): void => {
					const savedEventLogger = EventLogger.save(eventLogger);

					expect(EventLogger.load(savedEventLogger)).to.be.an.instanceOf(EventLogger);
				});
			});
		});

		describe('log', (): void => {
			const eventLogger = new EventLogger('test');
			const fight = new Fight([
				Characters.goblin(Team.player),
				Characters.goblin(Team.enemy)
			]);
			fight.victoryBy(Team.player);
			const testData = [
				{
					type: 'actionUsed',
					event: Events.actionUsed(
						Characters.goblin(Team.enemy), Actions.hit(), [Characters.goblin(Team.player)]
					)
				},
				{ type: 'characterDied', event: Events.characterDied(Characters.goblin(Team.player)) },
				{ type: 'victory', event: Events.victory(fight) }
			];

			function test(type: string, event: Event): void {
				it(`logs ${type} events`, (): void => {
					eventLogger.log(event);
					expect(eventLogger.getEvents()).to.includes(event);
				});
			}

			testData.forEach((data): void => test(data.type, data.event));
		});

		describe('getEvents', (): void => {
			const eventLogger = new EventLogger('test');

			const oldAction = getTestEvent(new Date(1993, 11, 25));
			const newAction = getTestEvent(new Date(1995, 3, 26));

			eventLogger.log(oldAction);
			eventLogger.log(newAction);

			const actions = eventLogger.getEvents(new Date(1994, 0, 0));

			expect(actions).to.not.include(oldAction);
			expect(actions).to.include(newAction);
		});
	});

	describe('Events', () => {
		it('have a getter for each event type', () => {
			const getters = Object.keys(Events);

			EventType.forEach((eventType) => {
				expect(getters).to.include(eventType);
			});
		});
	});
});
