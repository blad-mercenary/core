import { describe, it } from 'mocha';
import { expect } from 'chai';
import { Character, Team, SavedCharacter } from '../../lib/character';
import { Action, Target, TargetGroupIs, SelectDeadTargetIs } from '../../lib/action';
import { Effects } from '../../data/effect';

describe('Character', (): void => {
	describe('Character', (): void => {
		const character = new Character(
			'Test',
			'Test',
			Team.player,
			'Metal',
			['hit'],
			1,
			{},
			{}
		);

		describe('static', (): void => {
			describe('copy', (): void => {
				it('returns a identical copy of the given character', (): void => {
					const copy = Character.copy(character);

					expect(copy.name).to.equal(character.name);
					expect(copy.type).to.equal(character.type);
					expect(copy.team).to.equal(character.team);
					expect(copy.init).to.equal(character.init);
					expect(copy.health).to.equal(character.health);
					expect(copy.mana).to.equal(character.mana);
					expect(copy.actions).to.deep.equal(character.actions);
					expect(copy.lastTurn).to.equal(character.lastTurn);
					expect(copy.stats).to.deep.equal(character.stats);
				});

				it('returns different character', (): void => {
					const copy = Character.copy(character);

					expect(copy).to.not.equal(character);
					expect(copy.id).to.not.equal(character.id);
				});
			});

			describe('save', (): void => {
				it('returns a serializable object', (): void => {
					const savedCharacter = Character.save(character);

					expect(savedCharacter).to.be.an('object');
					expect((): string => { return JSON.stringify(savedCharacter); }).to.not.throw;
				});
			});

			describe('load', (): void => {
				it('returns a Character when called with a saved character', (): void => {
					const savedCharacter = Character.save(character);
					expect(Character.load(savedCharacter)).to.be.an.instanceOf(Character);
				});

				it('throws on non-saved character object', (): void => {
					expect((): void => {
						Character.load({} as unknown as SavedCharacter);
					}).to.throw();
				});
			});
		});

		describe('canUse', (): void => {
			it('returns true if the character have enought mana to use the action', (): void => {
				const action = new Action(
					'hit',
					'test',
					0,
					1,
					[Effects.fixedDamage({ damage: 1 })],
					Target.everyone,
					TargetGroupIs.impossible,
					SelectDeadTargetIs.allowed
				);

				expect(character.canUse(action)).to.be.true;
			});

			it('returns false if the character do not have enought mana to use the action', (): void => {
				const action = new Action(
					'hit',
					'test',
					0,
					1,
					[Effects.fixedDamage({ damage: 1 })],
					Target.everyone,
					TargetGroupIs.impossible,
					SelectDeadTargetIs.allowed
				);

				expect(character.canUse(action)).to.be.true;
			});
		});

		describe('isAlive', (): void => {
			it('returns true if the character have health', (): void => {
				expect(character.isAlive()).to.be.true;
			});

			it('returns false if the character do not have health', (): void => {
				const deadCharacter = new Character('Dead', 'Test', Team.enemy, 'Metal', [], 1, {}, {});
				deadCharacter.health = 0;
				expect(deadCharacter.isAlive()).to.be.false;
			});
		});
	});

	// describe('CharacterBuilder', (): void => {
	// 	describe('allows to create a character with only a team', (): void => {
	// 		Object.keys(StandardCharacterType).forEach((id): void => {
	// 			it(id, (): void => {
	// 				expect(
	// 					(StandardCharacterType[id as keyof StandardCharacterType] as CharacterGenerator)(Team.player)
	// 				).to.be.a.instanceOf(Character);
	// 				expect(
	// 					(StandardCharacterType[id as keyof StandardCharacterType] as CharacterGenerator)(Team.enemy)
	// 				).to.be.a.instanceOf(Character);
	// 			});
	// 		});
	// 	});
	// });
});
