import { describe, it } from 'mocha';
import { expect } from 'chai';

import { Selection } from '../../lib/selection';
import { Action, Target, TargetGroupIs, SelectDeadTargetIs } from '../../lib/action';
import { Character, Team } from '../../lib/character';
import { Fight } from '../../lib/fight';
import { Effects } from '../../data/effect';

describe('Selection', (): void => {
	const alivePlayer = new Character('Alive', 'Player', Team.player, 'Metal', ['hit'], 1, {}, {});
	const deadPlayer = new Character('Dead', 'Player', Team.player, 'Metal', ['hit'], 1, {}, {});
	deadPlayer.health = 0;
	const aliveEnemy = new Character('Alive', 'Enemy', Team.enemy, 'Metal', ['hit'], 1, {}, {});
	const deadEnemy = new Character('Dead', 'Enemy', Team.enemy, 'Metal', ['hit'], 1, {}, {});
	deadEnemy.health = 0;

	const fight = new Fight([alivePlayer, aliveEnemy, deadPlayer, deadEnemy]);
	const effects = [Effects.fixedDamage({ damage: 1 })];

	describe('getSelectionsForAction', (): void => {
		function getPossibleTargets(action: Action): Character[] {
			const possibleSelections = Selection.getSelectionsForAction(action, alivePlayer, fight);

			possibleSelections.forEach((s): void => {expect(s.targets.length).to.equal(1);});

			return possibleSelections.map((s): Character => s.targets[0]);
		}

		it('returns all targets if action allow to target everyone and dead targets', (): void => {
			const action = new Action(
				'hit',
				'test',
				0,
				0,
				effects,
				Target.everyone,
				TargetGroupIs.impossible,
				SelectDeadTargetIs.allowed
			);
			const possibleTargets = getPossibleTargets(action);

			expect(possibleTargets).to.include(alivePlayer);
			expect(possibleTargets).to.include(deadPlayer);
			expect(possibleTargets).to.include(aliveEnemy);
			expect(possibleTargets).to.include(deadEnemy);
		});

		it('returns all alive targets if action allow to target everyone and forbid dead targets', (): void => {
			const action = new Action(
				'hit', 'test', 0, 0, effects, Target.everyone, TargetGroupIs.impossible, SelectDeadTargetIs.disallowed
			);
			const possibleTargets = getPossibleTargets(action);

			expect(possibleTargets).to.include(alivePlayer);
			expect(possibleTargets).to.not.include(deadPlayer);
			expect(possibleTargets).to.include(aliveEnemy);
			expect(possibleTargets).to.not.include(deadEnemy);
		});

		it('returns all dead targets if action allow to target everyone and mandate dead targets', (): void => {
			const action = new Action(
				'hit', 'test', 0, 0, effects, Target.everyone, TargetGroupIs.impossible, SelectDeadTargetIs.mandatory
			);
			const possibleTargets = getPossibleTargets(action);

			expect(possibleTargets).to.not.include(alivePlayer);
			expect(possibleTargets).to.include(deadPlayer);
			expect(possibleTargets).to.not.include(aliveEnemy);
			expect(possibleTargets).to.include(deadEnemy);
		});

		it('returns all enemies targets if action allow to target enemies and dead targets', (): void => {
			const action = new Action(
				'hit', 'test', 0, 0, effects, Target.enemies, TargetGroupIs.impossible, SelectDeadTargetIs.allowed
			);
			const possibleTargets = getPossibleTargets(action);

			expect(possibleTargets).to.not.include(alivePlayer);
			expect(possibleTargets).to.not.include(deadPlayer);
			expect(possibleTargets).to.include(aliveEnemy);
			expect(possibleTargets).to.include(deadEnemy);
		});

		it('returns all alive enemies targets if action allow to target enemies and forbid dead targets', (): void => {
			const action = new Action(
				'hit', 'test', 0, 0, effects, Target.enemies, TargetGroupIs.impossible, SelectDeadTargetIs.disallowed
			);
			const possibleTargets = getPossibleTargets(action);

			expect(possibleTargets).to.not.include(alivePlayer);
			expect(possibleTargets).to.not.include(deadPlayer);
			expect(possibleTargets).to.include(aliveEnemy);
			expect(possibleTargets).to.not.include(deadEnemy);
		});

		it('returns all dead enemies targets if action allow to target enemies and mandate dead targets', (): void => {
			const action = new Action(
				'hit', 'test', 0, 0, effects, Target.enemies, TargetGroupIs.impossible, SelectDeadTargetIs.mandatory
			);
			const possibleTargets = getPossibleTargets(action);

			expect(possibleTargets).to.not.include(alivePlayer);
			expect(possibleTargets).to.not.include(deadPlayer);
			expect(possibleTargets).to.not.include(aliveEnemy);
			expect(possibleTargets).to.include(deadEnemy);
		});

		it('returns all allies targets if action allow to target allies and dead targets', (): void => {
			const action = new Action(
				'hit', 'test', 0, 0, effects, Target.allies, TargetGroupIs.impossible, SelectDeadTargetIs.allowed
			);
			const possibleTargets = getPossibleTargets(action);

			expect(possibleTargets).to.include(alivePlayer);
			expect(possibleTargets).to.include(deadPlayer);
			expect(possibleTargets).to.not.include(aliveEnemy);
			expect(possibleTargets).to.not.include(deadEnemy);
		});

		it('returns all alive allies targets if action allow to target allies and forbid dead targets', (): void => {
			const action = new Action(
				'hit', 'test', 0, 0, effects, Target.allies, TargetGroupIs.impossible, SelectDeadTargetIs.disallowed
			);
			const possibleTargets = getPossibleTargets(action);

			expect(possibleTargets).to.include(alivePlayer);
			expect(possibleTargets).to.not.include(deadPlayer);
			expect(possibleTargets).to.not.include(aliveEnemy);
			expect(possibleTargets).to.not.include(deadEnemy);
		});

		it('returns all dead allies targets if action allow to target allies and mandate dead targets', (): void => {
			const action = new Action(
				'hit', 'test', 0, 0, effects, Target.allies, TargetGroupIs.impossible, SelectDeadTargetIs.mandatory
			);
			const possibleTargets = getPossibleTargets(action);

			expect(possibleTargets).to.not.include(alivePlayer);
			expect(possibleTargets).to.include(deadPlayer);
			expect(possibleTargets).to.not.include(aliveEnemy);
			expect(possibleTargets).to.not.include(deadEnemy);
		});
	});

	describe('getSelectionsForGroupAction', (): void => {
		function getPossibleTargets(action: Action): { [index: string]: Character[] | undefined } {
			const possibleSelections = Selection.getSelectionsForGroupAction(action, alivePlayer, fight);

			possibleSelections.forEach((s): void => {
				const team = s.targets[0].team;
				expect(s.targets.every(
					(t): boolean => t.team === team
				)).to.be.true;
			});

			const alliesSelection = possibleSelections.find((s): boolean => s.targets[0].team === alivePlayer.team);
			const enemySelection = possibleSelections.find((s): boolean => s.targets[0].team !== alivePlayer.team);

			return {
				allies: alliesSelection? alliesSelection.targets : undefined,
				enemies: enemySelection? enemySelection.targets : undefined
			};
		}

		it(
			'returns all allies and enemies targets as groups if action allow to target everyone and dead targets',
			(): void => {
				const action = new Action(
					'hit', 'test', 0, 0, effects, Target.everyone, TargetGroupIs.mandatory, SelectDeadTargetIs.allowed
				);
				const { allies, enemies } = getPossibleTargets(action);

				expect(allies).to.not.be.undefined;
				expect(enemies).to.not.be.undefined;

				expect(allies).to.include(alivePlayer);
				expect(allies).to.include(deadPlayer);
				expect(enemies).to.include(aliveEnemy);
				expect(enemies).to.include(deadEnemy);
			}
		);

		it(
			'returns all alive targets as groups if action allow to target everyone and forbid dead targets',
			(): void => {
				const action = new Action(
					'hit',
					'test',
					0,
					0,
					effects,
					Target.everyone,
					TargetGroupIs.mandatory,
					SelectDeadTargetIs.disallowed
				);
				const { allies, enemies } = getPossibleTargets(action);

				expect(allies).to.not.be.undefined;
				expect(enemies).to.not.be.undefined;

				expect(allies).to.include(alivePlayer);
				expect(allies).to.not.include(deadPlayer);
				expect(enemies).to.include(aliveEnemy);
				expect(enemies).to.not.include(deadEnemy);
			}
		);

		it(
			'returns all dead targets as groups if action allow to target everyone and mandate dead targets',
			(): void => {
				const action = new Action(
					'hit', 'test', 0, 0, effects, Target.everyone, TargetGroupIs.mandatory, SelectDeadTargetIs.mandatory
				);
				const { allies, enemies } = getPossibleTargets(action);

				expect(allies).to.not.be.undefined;
				expect(enemies).to.not.be.undefined;

				expect(allies).to.not.include(alivePlayer);
				expect(allies).to.include(deadPlayer);
				expect(enemies).to.not.include(aliveEnemy);
				expect(enemies).to.include(deadEnemy);
			}
		);

		it('returns all enemies targets as group if action allow to target enemies and dead targets', (): void => {
			const action = new Action(
				'hit', 'test', 0, 0, effects, Target.enemies, TargetGroupIs.mandatory, SelectDeadTargetIs.allowed
			);
			const { allies, enemies } = getPossibleTargets(action);

			expect(allies).to.be.undefined;
			expect(enemies).to.not.be.undefined;

			expect(enemies).to.include(aliveEnemy);
			expect(enemies).to.include(deadEnemy);
		});

		it(
			'returns all alive enemies targets as group if action allow to target enemies and forbid dead targets',
			(): void => {
				const action = new Action(
					'hit', 'test', 0, 0, effects, Target.enemies, TargetGroupIs.mandatory, SelectDeadTargetIs.disallowed
				);
				const { allies, enemies } = getPossibleTargets(action);

				expect(allies).to.be.undefined;
				expect(enemies).to.not.be.undefined;

				expect(enemies).to.include(aliveEnemy);
				expect(enemies).to.not.include(deadEnemy);
			}
		);

		it(
			'returns all dead enemies targets as group if action allow to target enemies and mandate dead targets',
			(): void => {
				const action = new Action(
					'hit', 'test', 0, 0, effects, Target.enemies, TargetGroupIs.mandatory, SelectDeadTargetIs.mandatory
				);
				const { allies, enemies } = getPossibleTargets(action);

				expect(allies).to.be.undefined;
				expect(enemies).to.not.be.undefined;

				expect(enemies).to.not.include(aliveEnemy);
				expect(enemies).to.include(deadEnemy);
			}
		);

		it('returns all allies targets as group if action allow to target allies and dead targets', (): void => {
			const action = new Action(
				'hit',
				'test',
				0,
				0,
				[Effects.fixedDamage({ damage: 1 })],
				Target.allies,
				TargetGroupIs.mandatory,
				SelectDeadTargetIs.allowed
			);
			const { allies, enemies } = getPossibleTargets(action);

			expect(allies).to.not.be.undefined;
			expect(enemies).to.be.undefined;

			expect(allies).to.include(alivePlayer);
			expect(allies).to.include(deadPlayer);
		});

		it(
			'returns all alive allies targets as group if action allow to target allies and forbid dead targets',
			(): void => {
				const action = new Action(
					'hit', 'test', 0, 0, effects, Target.allies, TargetGroupIs.mandatory, SelectDeadTargetIs.disallowed
				);
				const { allies, enemies } = getPossibleTargets(action);

				expect(allies).to.not.be.undefined;
				expect(enemies).to.be.undefined;

				expect(allies).to.include(alivePlayer);
				expect(allies).to.not.include(deadPlayer);
			}
		);

		it(
			'returns all dead allies targets as group if action allow to target allies and mandate dead targets',
			(): void => {
				const action = new Action(
					'hit', 'test', 0, 0, effects, Target.allies, TargetGroupIs.mandatory, SelectDeadTargetIs.mandatory
				);
				const { allies, enemies } = getPossibleTargets(action);

				expect(allies).to.not.be.undefined;
				expect(enemies).to.be.undefined;

				expect(allies).to.not.include(alivePlayer);
				expect(allies).to.include(deadPlayer);
			}
		);
	});
});
