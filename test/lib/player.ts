import { describe, it } from 'mocha';
import { expect } from 'chai';
import { Player, SavedPlayer } from '../../lib/player';
import { EventLogger } from '../../lib/event';
import { Team } from '../../lib/character';
import { Characters } from '../../data/character';

describe('Player', (): void => {
	describe('Player', (): void => {
		function getPlayer(eventLogger: EventLogger = new EventLogger()): Player {
			return new Player(
				'test',
				'Test',
				1000,
				[Characters.goblin(Team.player)],
				eventLogger
			);
		}

		// describe('constructor', (): void => {

		// });

		describe('static', (): void => {
			describe('save', (): void => {
				it('returns a serializable object', (): void => {
					const savedPlayer = Player.save(getPlayer());

					expect(savedPlayer).to.be.an('object');
					expect((): string => { return JSON.stringify(savedPlayer); }).to.not.throw;
				});
			});

			describe('load', (): void => {
				it('returns a Player when called with a saved player', (): void => {
					const savedPlayer = Player.save(getPlayer());

					expect(Player.load(savedPlayer)).to.be.an.instanceOf(Player);
				});

				it('throws on non-saved player object', (): void => {
					expect((): void => {
						Player.load({} as unknown as SavedPlayer);
					}).to.throw();
				});
			});
		});
	});
});