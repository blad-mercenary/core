import { describe } from 'mocha';
import { expect } from 'chai';
import sinon from 'sinon';

import {
	Action,
	Target,
	SelectDeadTargetIs,
	TargetGroupIs
} from '../../lib/action';
import { Actions, ActionId } from '../../data/action';
import { Character, Team } from '../../lib/character';
import { Effects } from '../../data/effect';

describe('Action', (): void => {
	describe('Action', (): void => {
		// describe('constructor', (): void => {
		// });

		describe('static', (): void => {
			describe('save', (): void => {
				it('returns the id when saving a standard action', (): void => {
					const action = Actions.hit();
					expect(Action.save(action)).to.be.a('string');
				});
			});

			describe('load', (): void => {
				it('returns an Action when called with a standard action id', (): void => {
					const ActionsIds = Object.keys(Actions);

					ActionsIds.forEach((id): void => {
						expect(Action.load(id as ActionId)).to.be.an.instanceOf(Action);
					});
				});

				it('throws on invalid ids', (): void => {
					expect((): void => {
						Action.load('invalid action id' as ActionId);
					}).to.throw();
				});
			});
		});

		describe('apply', (): void => {
			it('inflict damage to the target', (): void => {
				const action = new Action(
					'hit',
					'Test',
					0,
					0,
					[Effects.fixedDamage({ damage: 1 })],
					Target.everyone,
					TargetGroupIs.impossible,
					SelectDeadTargetIs.allowed
				);
				const target = new Character('Test', 'Test', Team.enemy, 'Fire', [], 1, {}, {});
				const from = new Character('Test', 'Test', Team.enemy, 'Metal', [], 1, {}, {});
				const initialHealth = target.health;

				from.apply(action, target);

				expect(target.health).to.not.equal(initialHealth);
			});

			afterEach('sinon clean', (): void => {
				sinon.restore();
			});
		});

		describe('canTarget', (): void => {
			function getAction(
				possibleTarget: Target, groupAction: TargetGroupIs, deathSelection: SelectDeadTargetIs
			): Action {
				return new Action(
					'hit',
					'Test',
					1,
					0,
					[Effects.fixedDamage({ damage: 1 })],
					possibleTarget,
					groupAction,
					deathSelection
				);
			}

			function allow(action: Action, from: Character, target: Character): void {
				expect(from.canTarget(action, target)).to.be.true;
			}

			function disallow(action: Action, from: Character, target: Character): void {
				expect(from.canTarget(action, target)).to.be.false;
			}

			function getAlivePlayer(actions: Action[]): Character {
				const character = new Character('Alive', 'ally', Team.player, 'Metal', [], 1, {}, {});
				character.actions = actions;
				return character;
			}
			function getAliveEnemy(actions: Action[]): Character {
				const character = new Character('Alive', 'enemy', Team.enemy, 'Metal', [], 1, {}, {});
				character.actions = actions;
				return character;
			}
			function getDeadPlayer(actions: Action[]): Character {
				const character = new Character('Dead', 'ally', Team.player, 'Metal', [], 1, {}, {});
				character.health = 0;
				character.actions = actions;
				return character;
			}
			function getDeadEnemy(actions: Action[]): Character {
				const character = new Character('Dead', 'enemy', Team.enemy, 'Metal', [], 1, {}, {});
				character.health = 0;
				character.actions = actions;
				return character;
			}

			describe('target everyone', (): void => {
				describe('dead target allowed', (): void => {
					const action = getAction(Target.everyone, TargetGroupIs.impossible, SelectDeadTargetIs.allowed);
					const alivePlayer = getAlivePlayer([action]);
					const aliveEnemy = getAliveEnemy([action]);
					const deadPlayer = getDeadPlayer([action]);
					const deadEnemy = getDeadEnemy([action]);

					it('allow allied, alive target', (): void => {
						allow(action, alivePlayer, alivePlayer);
						allow(action, aliveEnemy, aliveEnemy);
					});
					it('allow allied, dead target', (): void => {
						allow(action, alivePlayer, deadPlayer);
						allow(action, aliveEnemy, deadEnemy);
					});
					it('allow enemy, alive target', (): void => {
						allow(action, alivePlayer, aliveEnemy);
						allow(action, aliveEnemy, alivePlayer);
					});
					it('allow enemy, dead target', (): void => {
						allow(action, alivePlayer, deadEnemy);
						allow(action, aliveEnemy, deadPlayer);
					});
				});

				describe('dead target disallowed', (): void => {
					const action = getAction(Target.everyone, TargetGroupIs.impossible, SelectDeadTargetIs.disallowed);
					const alivePlayer = getAlivePlayer([action]);
					const aliveEnemy = getAliveEnemy([action]);
					const deadPlayer = getDeadPlayer([action]);
					const deadEnemy = getDeadEnemy([action]);

					it('allow allied, alive target', (): void => {
						allow(action, alivePlayer, alivePlayer);
						allow(action, aliveEnemy, aliveEnemy);
					});
					it('disallow allied, dead target', (): void => {
						disallow(action, alivePlayer, deadPlayer);
						disallow(action, aliveEnemy, deadEnemy);
					});
					it('allow enemy, alive target', (): void => {
						allow(action, alivePlayer, aliveEnemy);
						allow(action, aliveEnemy, alivePlayer);
					});
					it('disallow enemy, dead target', (): void => {
						disallow(action, alivePlayer, deadEnemy);
						disallow(action, aliveEnemy, deadPlayer);
					});
				});

				describe('dead target only', (): void => {
					const action = getAction(Target.everyone, TargetGroupIs.impossible, SelectDeadTargetIs.mandatory);
					const alivePlayer = getAlivePlayer([action]);
					const aliveEnemy = getAliveEnemy([action]);
					const deadPlayer = getDeadPlayer([action]);
					const deadEnemy = getDeadEnemy([action]);

					it('disallow allied, alive target', (): void => {
						disallow(action, alivePlayer, alivePlayer);
						disallow(action, aliveEnemy, aliveEnemy);
					});
					it('allow allied, dead target', (): void => {
						allow(action, alivePlayer, deadPlayer);
						allow(action, aliveEnemy, deadEnemy);
					});
					it('disallow enemy, alive target', (): void => {
						disallow(action, alivePlayer, aliveEnemy);
						disallow(action, aliveEnemy, alivePlayer);
					});
					it('allow enemy, dead target', (): void => {
						allow(action, alivePlayer, deadEnemy);
						allow(action, aliveEnemy, deadPlayer);
					});
				});
			});

			describe('target allies', (): void => {
				describe('dead target allowed', (): void => {
					const action = getAction(Target.allies, TargetGroupIs.impossible, SelectDeadTargetIs.allowed);
					const alivePlayer = getAlivePlayer([action]);
					const aliveEnemy = getAliveEnemy([action]);
					const deadPlayer = getDeadPlayer([action]);
					const deadEnemy = getDeadEnemy([action]);

					it('allow allied, alive target', (): void => {
						allow(action, alivePlayer, alivePlayer);
						allow(action, aliveEnemy, aliveEnemy);
					});
					it('allow allied, dead target', (): void => {
						allow(action, alivePlayer, deadPlayer);
						allow(action, aliveEnemy, deadEnemy);
					});
					it('disallow enemy, alive target', (): void => {
						disallow(action, alivePlayer, aliveEnemy);
						disallow(action, aliveEnemy, alivePlayer);
					});
					it('disallow enemy, dead target', (): void => {
						disallow(action, alivePlayer, deadEnemy);
						disallow(action, aliveEnemy, deadPlayer);
					});
				});

				describe('dead target disallowed', (): void => {
					const action = getAction(Target.allies, TargetGroupIs.impossible, SelectDeadTargetIs.disallowed);
					const alivePlayer = getAlivePlayer([action]);
					const aliveEnemy = getAliveEnemy([action]);
					const deadPlayer = getDeadPlayer([action]);
					const deadEnemy = getDeadEnemy([action]);

					it('allow allied, alive target', (): void => {
						allow(action, alivePlayer, alivePlayer);
						allow(action, aliveEnemy, aliveEnemy);
					});
					it('disallow allied, dead target', (): void => {
						disallow(action, alivePlayer, deadPlayer);
						disallow(action, aliveEnemy, deadEnemy);
					});
					it('disallow enemy, alive target', (): void => {
						disallow(action, alivePlayer, aliveEnemy);
						disallow(action, aliveEnemy, alivePlayer);
					});
					it('disallow enemy, dead target', (): void => {
						disallow(action, alivePlayer, deadEnemy);
						disallow(action, aliveEnemy, deadPlayer);
					});
				});

				describe('dead target only', (): void => {
					const action = getAction(Target.allies, TargetGroupIs.impossible, SelectDeadTargetIs.mandatory);
					const alivePlayer = getAlivePlayer([action]);
					const aliveEnemy = getAliveEnemy([action]);
					const deadPlayer = getDeadPlayer([action]);
					const deadEnemy = getDeadEnemy([action]);

					it('disallow allied, alive target', (): void => {
						disallow(action, alivePlayer, alivePlayer);
						disallow(action, aliveEnemy, aliveEnemy);
					});
					it('allow allied, dead target', (): void => {
						allow(action, alivePlayer, deadPlayer);
						allow(action, aliveEnemy, deadEnemy);
					});
					it('disallow enemy, alive target', (): void => {
						disallow(action, alivePlayer, aliveEnemy);
						disallow(action, aliveEnemy, alivePlayer);
					});
					it('disallow enemy, dead target', (): void => {
						disallow(action, alivePlayer, deadEnemy);
						disallow(action, aliveEnemy, deadPlayer);
					});
				});
			});

			describe('target enemies', (): void => {
				describe('dead target allowed', (): void => {
					const action = getAction(Target.enemies, TargetGroupIs.impossible, SelectDeadTargetIs.allowed);
					const alivePlayer = getAlivePlayer([action]);
					const aliveEnemy = getAliveEnemy([action]);
					const deadPlayer = getDeadPlayer([action]);
					const deadEnemy = getDeadEnemy([action]);

					it('disallow allied, alive target', (): void => {
						disallow(action, alivePlayer, alivePlayer);
						disallow(action, aliveEnemy, aliveEnemy);
					});
					it('disallow allied, dead target', (): void => {
						disallow(action, alivePlayer, deadPlayer);
						disallow(action, aliveEnemy, deadEnemy);
					});
					it('allow enemy, alive target', (): void => {
						allow(action, alivePlayer, aliveEnemy);
						allow(action, aliveEnemy, alivePlayer);
					});
					it('allow enemy, dead target', (): void => {
						allow(action, alivePlayer, deadEnemy);
						allow(action, aliveEnemy, deadPlayer);
					});
				});

				describe('dead target disallowed', (): void => {
					const action = getAction(Target.enemies, TargetGroupIs.impossible, SelectDeadTargetIs.disallowed);
					const alivePlayer = getAlivePlayer([action]);
					const aliveEnemy = getAliveEnemy([action]);
					const deadPlayer = getDeadPlayer([action]);
					const deadEnemy = getDeadEnemy([action]);

					it('disallow allied, alive target', (): void => {
						disallow(action, alivePlayer, alivePlayer);
						disallow(action, aliveEnemy, aliveEnemy);
					});
					it('disallow allied, dead target', (): void => {
						disallow(action, alivePlayer, deadPlayer);
						disallow(action, aliveEnemy, deadEnemy);
					});
					it('allow enemy, alive target', (): void => {
						allow(action, alivePlayer, aliveEnemy);
						allow(action, aliveEnemy, alivePlayer);
					});
					it('disallow enemy, dead target', (): void => {
						disallow(action, alivePlayer, deadEnemy);
						disallow(action, aliveEnemy, deadPlayer);
					});
				});

				describe('dead target only', (): void => {
					const action = getAction(Target.enemies, TargetGroupIs.impossible, SelectDeadTargetIs.mandatory);
					const alivePlayer = getAlivePlayer([action]);
					const aliveEnemy = getAliveEnemy([action]);
					const deadPlayer = getDeadPlayer([action]);
					const deadEnemy = getDeadEnemy([action]);

					it('disallow allied, alive target', (): void => {
						disallow(action, alivePlayer, alivePlayer);
						disallow(action, aliveEnemy, aliveEnemy);
					});
					it('disallow allied, dead target', (): void => {
						disallow(action, alivePlayer, deadPlayer);
						disallow(action, aliveEnemy, deadEnemy);
					});
					it('disallow enemy, alive target', (): void => {
						disallow(action, alivePlayer, aliveEnemy);
						disallow(action, aliveEnemy, alivePlayer);
					});
					it('allow enemy, dead target', (): void => {
						allow(action, alivePlayer, deadEnemy);
						allow(action, aliveEnemy, deadPlayer);
					});
				});
			});
		});
	});
});
