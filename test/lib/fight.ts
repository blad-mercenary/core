import { describe, it } from 'mocha';
import { expect } from 'chai';
import { Character, Team } from '../../lib/character';
import { Fight, SavedFight } from '../../lib/fight';
import { Selection } from '../../lib/selection';
import { Actions } from '../../data/action';
import sinon from 'sinon';

function getCharacter(team: Team, alive: boolean, init = 1, name = 'Test'): Character {
	const character = new Character(name, 'Test', team, 'Metal', ['hit'], 1, {}, {});
	character.init = init;
	if (!alive) {
		character.health = 0;
	}
	return character;
}

const alive = true;
const dead = false;

describe('Fight', () => {
	describe('Fight', () => {
		describe('constructor', () => {
			it('reject empty fighters list', () => {
				expect(() => {
					new Fight([]);
				}).to.throw();
			});

			it('reject fighters list with no player characters', () => {
				expect(() => {
					new Fight([getCharacter(Team.enemy, alive)]);
				}).to.throw();
			});

			it('reject fighters list with no enemy characters', () => {
				expect(() => {
					new Fight([getCharacter(Team.player, alive)]);
				}).to.throw();
			});
		});

		describe('static', () => {
			const fight = new Fight([
				getCharacter(Team.player, alive),
				getCharacter(Team.enemy, alive)
			]);

			describe('save', () => {
				it('returns a serializable object', () => {
					const savedFight = Fight.save(fight);

					expect(savedFight).to.be.an('object');
					expect((): string => { return JSON.stringify(savedFight); }).to.not.throw;
				});
			});

			describe('load', () => {
				it('returns a Fight when called with a saved unfinished fight', () => {
					const savedFight = Fight.save(fight);

					expect(Fight.load(savedFight)).to.be.an.instanceOf(Fight);
				});

				it('returns a Fight when called with a saved finished fight', () => {
					const fight = new Fight([
						getCharacter(Team.player, alive),
						getCharacter(Team.enemy, alive)
					]);
					fight.victoryBy(Team.player);
					const savedFight = Fight.save(fight);

					expect(Fight.load(savedFight)).to.be.an.instanceOf(Fight);
				});

				it('throws on non-saved fight object', () => {
					expect(() => {
						Fight.load({} as unknown as SavedFight);
					}).to.throw();
				});
			});
		});

		describe('getActiveFighter', () => {
			it('always returns a live character', () => {
				const active = getCharacter(Team.player, alive, 100, 'Active');

				const fight = new Fight([
					getCharacter(Team.enemy, dead, 0),
					getCharacter(Team.enemy, alive, 1000),
					getCharacter(Team.enemy, dead, 1),
					active,
					getCharacter(Team.enemy, dead, 2)
				]);

				expect(fight.getActiveFighter()).to.equals(active);
			});

			it('sets the init of the returned character to 0', () => {
				const fight = new Fight([
					getCharacter(Team.player, alive, 1000),
					getCharacter(Team.enemy, alive, 10000)
				]);

				const activeFighter = fight.getActiveFighter();

				expect(activeFighter.init).to.equals(0);
			});

			it('keeps the init of other character the same relative to the returned one', () => {
				const fighters = [
					getCharacter(Team.player, alive, 1, 'Active'),
					getCharacter(Team.enemy, alive, 5, '4'),
					getCharacter(Team.enemy, alive, 10, '9'),
					getCharacter(Team.enemy, alive, 100, '99'),
					getCharacter(Team.enemy, alive, 220, '219'),
					getCharacter(Team.enemy, alive, 10000, '9999'),
					getCharacter(Team.enemy, alive, 100000, '99999')
				];

				const fight = new Fight(fighters);

				const activeFighter = fight.getActiveFighter();

				function get(name: string): Character {
					const c = fight.fighters.find((f): boolean => f.name === name);
					if (c === undefined) { throw new Error(`Didn't found ${name}`);}
					return c;
				}

				expect(activeFighter).to.equals(get('Active'));

				expect(get('Active').init).to.equals(0);
				expect(get('4').init).to.equals(4);
				expect(get('9').init).to.equals(9);
				expect(get('99').init).to.equals(99);
				expect(get('219').init).to.equals(219);
				expect(get('9999').init).to.equals(9999);
				expect(get('99999').init).to.equals(99999);
			});

			it('do not touch the init if the active character already had its init at 0', () => {
				const fight = new Fight([
					getCharacter(Team.player, alive, 0, 'p0'),
					getCharacter(Team.enemy, alive, 0, 'e0'),
					getCharacter(Team.player, alive, 219, 'p'),
					getCharacter(Team.enemy, alive, 523, 'e')
				]);

				fight.getActiveFighter();

				function get(name: string): Character {
					const c = fight.fighters.find((f): boolean => f.name === name);
					if (c === undefined) { throw new Error(`Didn't found ${name}`);}
					return c;
				}

				expect(get('p0').init).to.equals(0);
				expect(get('e0').init).to.equals(0);
				expect(get('p').init).to.equals(219);
				expect(get('e').init).to.equals(523);
			});

			it('returns the character who have had their turn less recently on equal init', () => {
				const fight = new Fight([
					getCharacter(Team.player, alive, 0, 'p0'),
					getCharacter(Team.enemy, alive, 0, 'e0')
				]);

				fight.fighters[0].init = 100;
				fight.fighters[1].init = 100;
				fight.fighters[0].lastTurn = 10;
				fight.fighters[1].lastTurn = 0;

				const activeFighter = fight.getActiveFighter();

				expect(activeFighter).to.equals(fight.fighters[0]);
			});

			it('returns the character with first id alphabeticaly on equal init and last turn', () => {
				const fight = new Fight([
					getCharacter(Team.player, alive, 0, 'p0'),
					getCharacter(Team.enemy, alive, 0, 'e0')
				]);

				fight.fighters[0].init = 100;
				fight.fighters[1].init = 100;
				fight.fighters[0].lastTurn = Number.POSITIVE_INFINITY;
				fight.fighters[1].lastTurn = Number.POSITIVE_INFINITY;
				fight.fighters[0].id = 'AAA';
				fight.fighters[1].id = 'ZZZ';

				const activeFighter = fight.getActiveFighter();

				expect(activeFighter).to.equals(fight.fighters[0]);
			});

			it('stays consistent no matter how many time it is called successively', () => {
				const fight = new Fight([
					getCharacter(Team.player, alive, 0, 'p0'),
					getCharacter(Team.enemy, alive, 0, 'e0'),
					getCharacter(Team.player, alive, 0, 'p0'),
					getCharacter(Team.enemy, alive, 0, 'e0'),
					getCharacter(Team.player, alive, 0, 'p0'),
					getCharacter(Team.enemy, alive, 0, 'e0')
				]);

				const activeFighter = fight.getActiveFighter();

				let i = 0;

				while (i < 1000) {
					expect(fight.getActiveFighter()).to.equal(activeFighter);
					i += 1;
				}
			});

			it('stays consistent no matter how many time it is called successively through reloads', () => {
				function reload(fight: Fight): Fight {
					return Fight.load(JSON.parse(
						JSON.stringify(Fight.save(fight))
					));
				}

				let fight = new Fight([
					getCharacter(Team.player, alive, 0, 'p0'),
					getCharacter(Team.enemy, alive, 0, 'e0'),
					getCharacter(Team.player, alive, 0, 'p0'),
					getCharacter(Team.enemy, alive, 0, 'e0'),
					getCharacter(Team.player, alive, 0, 'p0'),
					getCharacter(Team.enemy, alive, 0, 'e0')
				]);

				const activeFighter = fight.getActiveFighter();

				let i = 0;

				while (i < 100) {
					fight = reload(fight);
					expect(fight.getActiveFighter()).to.deep.equal(activeFighter);
					i += 1;
				}
			});

			it('throws if there are no valid characters', () => {
				const characters = [
					getCharacter(Team.player, dead),
					getCharacter(Team.enemy, dead)
				];
				const fight = new Fight(characters);
				characters.forEach((c) => {
					c.health = 0;
				});

				expect(() => {
					fight.getActiveFighter();
				}).to.throw();
			});
		});

		describe('hasFighterAliveIn', () => {
			describe('returns true if there is a least one fighter alive in the given team', () => {
				const characters = [
					getCharacter(Team.player, alive),
					getCharacter(Team.enemy, alive)
				];
				const fight = new Fight(characters);

				it('player', () => {
					expect(fight.hasFighterAliveIn(Team.player)).to.be.true;
				});
				it('enemy', () => {
					expect(fight.hasFighterAliveIn(Team.enemy)).to.be.true;
				});
			});

			describe('returns false if there is no fighters alive in the given team', () => {
				const characters = [
					getCharacter(Team.player, dead),
					getCharacter(Team.enemy, dead)
				];
				const fight = new Fight(characters);

				characters.forEach((c) => {
					c.health = 0;
				});

				it('player', () => {
					expect(fight.hasFighterAliveIn(Team.player)).to.be.false;
				});
				it('enemy', () => {
					expect(fight.hasFighterAliveIn(Team.enemy)).to.be.false;
				});
			});
		});

		describe('wonBy', () => {
			describe('set the flags to indicate victory by the given team', () => {
				it('player', () => {
					const fight = new Fight([
						getCharacter(Team.player, alive),
						getCharacter(Team.enemy, alive)
					]);

					fight.victoryBy(Team.player);

					expect(fight.finished).to.be.true;
					expect(fight.winner).to.equal(Team.player);
				});

				it('enemy', () => {
					const fight = new Fight([
						getCharacter(Team.player, alive),
						getCharacter(Team.enemy, alive)
					]);

					fight.victoryBy(Team.enemy);

					expect(fight.finished).to.be.true;
					expect(fight.winner).to.equal(Team.enemy);
				});
			});
		});

		describe('validateSelection', () => {
			beforeEach(() => {
				sinon.stub();
			});

			it('returns selection object if valid', () => {
				const characters = [
					getCharacter(Team.player, true),
					getCharacter(Team.enemy, true)
				];
				const fight = new Fight(characters);
				const activeFighter = fight.getActiveFighter();
				const selection = Selection.getPossibleSelections(activeFighter, fight)[0];

				const result = fight.validateSelection(activeFighter, selection);

				expect(result).to.deep.equal(selection);
			});

			it('return selection object from index if valid', () => {
				const characters = [
					getCharacter(Team.player, true),
					getCharacter(Team.enemy, true)
				];
				const fight = new Fight(characters);
				const activeFighter = fight.getActiveFighter();
				const selection = Selection.getPossibleSelections(activeFighter, fight)[0];

				const result = fight.validateSelection(activeFighter, 0);

				expect(result).to.deep.equal(selection);
			});

			it('returns a random selection if given undefined and there are possible actions', () => {
				const characters = [
					getCharacter(Team.player, true),
					getCharacter(Team.enemy, true)
				];
				const fight = new Fight(characters);

				const result = fight.validateSelection(fight.getActiveFighter());

				expect(Selection.isSelection(result)).to.be.true;
			});

			it('returns undefined if given undefined and there is no possible actions', () => {
				sinon.stub(Selection, 'validate');
				const characters = [
					getCharacter(Team.player, true),
					getCharacter(Team.enemy, false)
				];
				const fight = new Fight(characters);

				const result = fight.validateSelection(fight.getActiveFighter());

				expect(result).to.deep.equal(undefined);
			});

			it('rejects invalid selection object', () => {
				const characters = [
					getCharacter(Team.player, true),
					getCharacter(Team.enemy, true)
				];
				const fight = new Fight(characters);
				const selection = { action: Actions.hit(), targets: [getCharacter(Team.player, true)] };

				expect(
					() => fight.validateSelection(fight.getActiveFighter(), selection)
				).to.throw(Error, `Invalid selection ${JSON.stringify(selection)}`);
			});

			it('rejects invalid selection index', () => {
				const characters = [
					getCharacter(Team.player, true),
					getCharacter(Team.enemy, true)
				];
				const fight = new Fight(characters);

				expect(() => fight.validateSelection(
					fight.getActiveFighter(), -1
				)).to.throw(Error, `Invalid selection index ${-1}`);
			});
		});
	});
});