import { expect } from 'chai';
import { Characters, Effects } from '../../data';
import { Action, SelectDeadTargetIs, Target, TargetGroupIs } from '../../lib/action';
import { Character, Team } from '../../lib/character';
import { Effect } from '../../lib/effects';

describe('EffectImplementation', (): void => {
	function getAction(effect: Effect): Action {
		return new Action(
			'hit', 'test', 1, 1, [effect], Target.enemies, TargetGroupIs.mandatory, SelectDeadTargetIs.disallowed
		);
	}
	function getPlayer(action: Action): Character {
		const character = Characters.goblin(Team.player, 'player');
		character.actions = [action];
		return character;
	}

	describe('fixed damage', (): void => {
		it('inflict fixed damage to the target', (): void => {
			const action = getAction(Effects.fixedDamage({ damage: 1 }));
			const from = getPlayer(action);
			const target = Characters.goblin(Team.enemy);

			from.apply(action, target);

			expect(target.health).to.equal(target.maxHealth - 1);
		});
	});
});