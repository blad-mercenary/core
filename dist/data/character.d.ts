import { Character, Team } from '../lib/character';
import { ActionId } from './action';
export declare type CharacterType = 'goblin' | 'warrior' | 'mage' | 'healer';
export declare const CharacterType: string[];
export interface CharacterTypeData {
    type: string;
    name: string[];
    baseInit: number;
    maxHealth: number;
    maxMana: number;
    actions: ActionId[];
}
declare type CharacterBuilder = (team: Team, name?: string) => Character;
export declare const defaultCharacterNames: Record<CharacterType, string[]>;
export declare const Characters: Record<CharacterType, CharacterBuilder>;
export declare function isCharacterType(value: string): value is CharacterType;
export {};
