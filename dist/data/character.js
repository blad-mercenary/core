"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isCharacterType = exports.Characters = exports.defaultCharacterNames = exports.CharacterType = void 0;
var character_1 = require("../lib/character");
var utils_1 = require("../lib/utils");
exports.CharacterType = ['goblin', 'warrior', 'mage', 'healer'];
function getRandomNameFrom(names) {
    return names[(0, utils_1.getRandomInt)(0, names.length - 1)];
}
exports.defaultCharacterNames = {
    goblin: [
        'Wril',
        'Tras',
        'Olk',
        'Akz',
        'Glibs',
        'Bleehbior',
        'Vigmioz',
        'Peegkoz',
        'Vreegniarm',
        'Vrozbalx'
    ],
    warrior: [
        'Asbjørn Jan',
        'Christoffer Tore',
        'Truls Rúnar',
        'Fergie Lucas',
        'Rúnar Sune'
    ],
    mage: [
        'Mághnus',
        'Séamus',
        'Shamus',
        'Ionatán',
        'Muirín'
    ],
    healer: [
        'Ĉiela',
        'Karesinda',
        'Adorinda',
        'Gaja',
        'Nikolao'
    ]
};
exports.Characters = {
    goblin: function (team, name, level) {
        if (level === void 0) { level = 1; }
        if (!name) {
            name = getRandomNameFrom(exports.defaultCharacterNames.goblin);
        }
        return new character_1.Character(name, 'goblin', team, 'Wood', ['skip', 'hit'], level, { hp: 10, mp: 5 }, { hp: 1.1, mp: 1.05, atk: 1.15, def: 1.15, speAtk: 1.05, speDef: 1.05 });
    },
    warrior: function (team, name, level) {
        if (level === void 0) { level = 1; }
        if (!name) {
            name = getRandomNameFrom(exports.defaultCharacterNames.warrior);
        }
        return new character_1.Character(name, 'warrior', team, 'Fire', ['skip', 'hit'], level, { hp: 20, mp: 5, atk: 5, def: 3 }, { hp: 1.1, mp: 1.05, atk: 1.1, def: 1.1, speAtk: 1.05, speDef: 1.05 });
    },
    mage: function (team, name, level) {
        if (level === void 0) { level = 1; }
        if (!name) {
            name = getRandomNameFrom(exports.defaultCharacterNames.mage);
        }
        return new character_1.Character(name, 'mage', team, 'Water', ['skip', 'ice shard', 'ice wave'], level, { hp: 10, mp: 10, speAtk: 5, speDef: 3 }, { hp: 1.05, mp: 1.1, speAtk: 1.1, speDef: 1.05 });
    },
    healer: function (team, name, level) {
        if (level === void 0) { level = 1; }
        if (!name) {
            name = getRandomNameFrom(exports.defaultCharacterNames.healer);
        }
        return new character_1.Character(name, 'healer', team, 'Earth', ['skip', 'hit', 'heal', 'raise'], level, { hp: 10, mp: 10, speAtk: 3, speDef: 5 }, { hp: 1.05, mp: 1.1, speAtk: 1.05, speDef: 1.1 });
    }
};
function isCharacterType(value) {
    return exports.CharacterType.includes(value);
}
exports.isCharacterType = isCharacterType;
