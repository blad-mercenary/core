import { Action } from '../lib/action';
export declare const Actions: {
    skip: () => Action;
    hit: () => Action;
    'ice shard': () => Action;
    'ice wave': () => Action;
    heal: () => Action;
    raise: () => Action;
};
export declare type ActionId = keyof typeof Actions;
export declare function isActionId(value: string): value is ActionId;
