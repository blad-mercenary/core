import { Character } from '../lib/character';
import { Effect } from '../lib/effects';
import { Element } from '../lib/elements';
import { Event } from '../lib/event';
export interface FixedDamageDetails {
    damage: number;
}
export declare type PhysicalDamageDetails = {
    type: 'Physical';
} & FixedDamageDetails;
export declare function isPhysicalDamageDetails(details: EffectDetails): details is PhysicalDamageDetails;
export declare type ElementalDamageDetails = {
    type: 'Special';
    element: Element;
} & FixedDamageDetails;
export declare function isElementalDamageDetails(details: EffectDetails): details is ElementalDamageDetails;
export declare type DamageDetails = PhysicalDamageDetails | ElementalDamageDetails;
export interface HealDetails {
    heal: number;
}
export declare type EffectDetails = (FixedDamageDetails | DamageDetails | HealDetails);
export declare type EffectImplementation = (target: Character, from: Character, details: EffectDetails) => Event;
export declare type EffectType = 'fixedDamage' | 'damage' | 'fixedHeal' | 'heal' | 'resurrect';
export declare const EffectImplementations: Record<EffectType, EffectImplementation>;
export declare const Effects: {
    fixedDamage: (details: FixedDamageDetails) => Effect;
    damage: (details: DamageDetails) => Effect;
    fixedHeal: (details: HealDetails) => Effect;
    heal: (details: HealDetails) => Effect;
    resurrect: (details: HealDetails) => Effect;
};
