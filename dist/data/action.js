"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isActionId = exports.Actions = void 0;
var action_1 = require("../lib/action");
var effect_1 = require("./effect");
exports.Actions = {
    skip: function () {
        return new action_1.Action('skip', 'Skip', 1, 0, [], action_1.Target.self, action_1.TargetGroupIs.impossible, action_1.SelectDeadTargetIs.disallowed);
    },
    hit: function () {
        return new action_1.Action('hit', 'Hit', 3, 0, [effect_1.Effects.damage({ damage: 1, type: 'Physical' })], action_1.Target.enemies, action_1.TargetGroupIs.impossible, action_1.SelectDeadTargetIs.disallowed);
    },
    'ice shard': function () {
        return new action_1.Action('ice shard', 'Ice Shard', 3, 1, [effect_1.Effects.damage({ damage: 2, type: 'Special', element: 'Water' })], action_1.Target.enemies, action_1.TargetGroupIs.impossible, action_1.SelectDeadTargetIs.disallowed);
    },
    'ice wave': function () {
        return new action_1.Action('ice wave', 'Ice Wave', 5, 4, [effect_1.Effects.damage({ damage: 2, type: 'Special', element: 'Water' })], action_1.Target.enemies, action_1.TargetGroupIs.mandatory, action_1.SelectDeadTargetIs.disallowed);
    },
    heal: function () {
        return new action_1.Action('heal', 'Heal', 3, 3, [], action_1.Target.allies, action_1.TargetGroupIs.impossible, action_1.SelectDeadTargetIs.disallowed);
    },
    raise: function () {
        return new action_1.Action('raise', 'Raise', 10, 5, [effect_1.Effects.resurrect({ heal: 3 })], action_1.Target.allies, action_1.TargetGroupIs.impossible, action_1.SelectDeadTargetIs.mandatory);
    }
};
var validActionIds = Object.keys(exports.Actions);
function isActionId(value) {
    return validActionIds.includes(value);
}
exports.isActionId = isActionId;
