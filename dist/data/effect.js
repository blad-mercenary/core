"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Effects = exports.EffectImplementations = exports.isElementalDamageDetails = exports.isPhysicalDamageDetails = void 0;
var effects_1 = require("../lib/effects");
var elements_1 = require("../lib/elements");
var event_1 = require("../lib/event");
function isFixedDamageDetails(details) {
    return !!details && 'damage' in details;
}
function isPhysicalDamageDetails(details) {
    return 'type' in details && details.type === 'Physical';
}
exports.isPhysicalDamageDetails = isPhysicalDamageDetails;
function isElementalDamageDetails(details) {
    return 'type' in details && details.type === 'Special' && (0, elements_1.isElement)(details.element);
}
exports.isElementalDamageDetails = isElementalDamageDetails;
function isDamageDetails(details) {
    return !!details
        && 'damage' in details
        && 'type' in details
        && (details.type === 'Physical' ||
            (details.type === 'Special' && 'element' in details && (0, elements_1.isElement)(details.element)));
}
function isHealDetails(details) {
    return !!details && ('heal' in details);
}
function formula(power, atk, def, weaknessFactor) {
    return Math.floor(((power * atk / def) + 1) * weaknessFactor);
}
exports.EffectImplementations = {
    fixedDamage: function (target, from, details) {
        if (!isFixedDamageDetails(details)) {
            throw new Error("Wrong details given for damage ".concat(JSON.stringify(details)));
        }
        target.health -= details.damage;
        if (target.health > target.maxHealth) {
            target.health = target.maxHealth;
        }
        else if (target.health < 0) {
            target.health = 0;
        }
        return event_1.Events.damage(from, target, details.damage);
    },
    damage: function (target, from, details) {
        if (!isDamageDetails(details)) {
            throw new Error("Wrong details given for damage ".concat(JSON.stringify(details)));
        }
        var atk, def, weaknessFactor;
        if (details.type === 'Physical') {
            atk = from.stats.atk;
            def = target.stats.def;
            weaknessFactor = 1;
        }
        else if (details.type === 'Special') {
            atk = from.stats.speAtk;
            def = target.stats.speDef;
            if (elements_1.elementalWeakness[target.element] === details.element) {
                weaknessFactor = 2;
            }
            else if (elements_1.elementalWeakness[details.element] === target.element) {
                weaknessFactor = 0.5;
            }
            else {
                weaknessFactor = 1;
            }
        }
        else {
            throw new Error("Invalid details ".concat(JSON.stringify(details)));
        }
        var damage = formula(details.damage, atk, def, weaknessFactor);
        target.health -= damage;
        if (target.health > target.maxHealth) {
            target.health = target.maxHealth;
        }
        else if (target.health < 0) {
            target.health = 0;
        }
        return event_1.Events.damage(from, target, damage);
    },
    fixedHeal: function (target, from, details) {
        if (!isHealDetails(details)) {
            throw new Error("Wrong details given for heal ".concat(JSON.stringify(details)));
        }
        target.health += details.heal;
        return event_1.Events.heal(from, target, details.heal);
    },
    heal: function (target, from, details) {
        if (!isHealDetails(details)) {
            throw new Error("Wrong details given for heal ".concat(JSON.stringify(details)));
        }
        target.health += formula(details.heal, from.stats.speDef, 1, 1);
        return event_1.Events.heal(from, target, details.heal);
    },
    resurrect: function (target, from, details) {
        if (!isHealDetails(details)) {
            throw new Error("Wrong details given for heal ".concat(JSON.stringify(details)));
        }
        exports.EffectImplementations.heal(target, from, details);
        return event_1.Events.resurrect(from, target);
    }
};
exports.Effects = {
    fixedDamage: function (details) {
        return new effects_1.Effect('fixedDamage', details, exports.EffectImplementations.fixedDamage);
    },
    damage: function (details) {
        return new effects_1.Effect('damage', details, exports.EffectImplementations.damage);
    },
    fixedHeal: function (details) {
        return new effects_1.Effect('fixedHeal', details, exports.EffectImplementations.fixedHeal);
    },
    heal: function (details) {
        return new effects_1.Effect('heal', details, exports.EffectImplementations.heal);
    },
    resurrect: function (details) {
        return new effects_1.Effect('resurrect', details, exports.EffectImplementations.resurrect);
    }
};
