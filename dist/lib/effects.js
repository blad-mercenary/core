"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Effect = void 0;
var Effect = (function () {
    function Effect(type, details, apply) {
        this.type = type;
        this.details = details;
        this._apply = apply;
    }
    Effect.prototype.apply = function (target, from) {
        return this._apply(target, from, this.details);
    };
    return Effect;
}());
exports.Effect = Effect;
