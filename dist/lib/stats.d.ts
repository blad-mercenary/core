export interface FullStatsSet {
    hp: number;
    mp: number;
    atk: number;
    def: number;
    speAtk: number;
    speDef: number;
    init: number;
}
export declare function isFullStatsSet(saved: any): saved is FullStatsSet;
export interface StatsSet {
    hp?: number;
    mp?: number;
    atk?: number;
    def?: number;
    speAtk?: number;
    speDef?: number;
    init?: number;
}
export declare function toFullStatsSet(partial: StatsSet, defaultStat?: number): FullStatsSet;
export declare const statTypes: StatType[];
export declare type StatType = keyof FullStatsSet;
export interface SavedModifier {
    value: FullStatsSet;
    expires?: number;
}
export declare function isSavedModifier(saved: any): saved is SavedModifier;
export interface SavedStats {
    initial: FullStatsSet;
    affinity: FullStatsSet;
    modifiers: SavedModifier[];
    level: number;
}
export declare function isSavedStats(saved: any): saved is SavedStats;
export declare class StatModifier {
    value: FullStatsSet;
    expires?: number;
    constructor(value: StatsSet, expires?: number);
}
export declare class Stats {
    private initial;
    private affinity;
    private _modifiers;
    private level;
    private statsCache;
    constructor(init: StatsSet, affinity: StatsSet, level: number, modifiers?: StatModifier[]);
    protected save(): SavedStats;
    static save(stats: Stats): SavedStats;
    static load(saved: SavedStats): Stats;
    compute(level?: number): void;
    get modifiers(): StatModifier[];
    set modifiers(modifiers: StatModifier[]);
    step(): void;
    get(stat: StatType): number;
    get hp(): number;
    get mp(): number;
    get atk(): number;
    get def(): number;
    get speAtk(): number;
    get speDef(): number;
    get init(): number;
}
