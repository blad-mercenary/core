"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Selection = void 0;
var character_1 = require("./character");
var action_1 = require("./action");
function isSelection(obj) {
    return (typeof obj === 'object'
        && obj.action && obj.action instanceof action_1.Action
        && obj.targets && Array.isArray(obj.targets) && obj.targets.every(function (target) { return target instanceof character_1.Character; }));
}
function getSelectionsForAction(action, character, fight) {
    var potentialTargets;
    if (action.possibleTarget === action_1.Target.self) {
        potentialTargets = [character];
    }
    else if (action.deathSelection === action_1.SelectDeadTargetIs.allowed) {
        potentialTargets = fight.fighters;
    }
    else if (action.deathSelection === action_1.SelectDeadTargetIs.disallowed) {
        potentialTargets = fight.fighters.filter(function (f) { return f.isAlive(); });
    }
    else if (action.deathSelection === action_1.SelectDeadTargetIs.mandatory) {
        potentialTargets = fight.fighters.filter(function (f) { return !f.isAlive(); });
    }
    else {
        throw new Error("Invalid deathSelection ".concat(action.deathSelection));
    }
    return potentialTargets
        .filter(function (fighter) { return character.canTarget(action, fighter); })
        .map(function (fighter) { return ({ action: action, targets: [fighter] }); });
}
function getSelectionsForGroupAction(action, character, fight) {
    var allies = fight.fighters.filter(function (fighter) { return fighter.team === character.team; });
    var enemies = fight.fighters.filter(function (fighter) { return fighter.team !== character.team; });
    function select(group) {
        return group.filter(function (c) { return character.canTarget(action, c); });
    }
    var selections = [];
    if (action.possibleTarget === action_1.Target.self) {
        selections.push({ action: action, targets: [character] });
    }
    else if (action.possibleTarget === action_1.Target.everyone) {
        selections.push({ action: action, targets: select(allies) });
        selections.push({ action: action, targets: select(enemies) });
    }
    else if (action.possibleTarget === action_1.Target.allies) {
        selections.push({ action: action, targets: select(allies) });
    }
    else if (action.possibleTarget === action_1.Target.enemies) {
        selections.push({ action: action, targets: select(enemies) });
    }
    return selections.filter(function (s) { return s.targets.length > 0; });
}
function getPossibleSelections(character, fight) {
    var possibleActions = character.actions.filter(function (action) { return character.canUse(action); });
    return possibleActions.map(function (action) {
        if (action.groupAction === action_1.TargetGroupIs.impossible) {
            return getSelectionsForAction(action, character, fight);
        }
        else {
            return getSelectionsForGroupAction(action, character, fight);
        }
    }).reduce(function (acc, cur) { return acc.concat(cur); }, []);
}
function validate(selection, character, fight) {
    var possibleActions = getPossibleSelections(character, fight);
    var selectionTargets = selection.targets.map(function (character) { return character.id; });
    return possibleActions.some(function (possible) {
        var possibleTargets = possible.targets.map(function (character) { return character.id; });
        return selection.action === possible.action
            && selectionTargets.every(function (character) { return possibleTargets.includes(character); })
            && possibleTargets.every(function (character) { return selectionTargets.includes(character); });
    });
}
exports.Selection = {
    isSelection: isSelection,
    getSelectionsForAction: getSelectionsForAction,
    getSelectionsForGroupAction: getSelectionsForGroupAction,
    getPossibleSelections: getPossibleSelections,
    validate: validate
};
