"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Character = exports.isSavedCharacter = exports.Team = void 0;
var action_1 = require("./action");
var action_2 = require("../data/action");
var uuid_1 = require("uuid");
var event_1 = require("./event");
var stats_1 = require("./stats");
var elements_1 = require("./elements");
var Team;
(function (Team) {
    Team["player"] = "player";
    Team["enemy"] = "enemy";
})(Team = exports.Team || (exports.Team = {}));
function isSavedCharacter(value) {
    return (typeof value === 'object'
        && typeof (value === null || value === void 0 ? void 0 : value.id) === 'string'
        && typeof (value === null || value === void 0 ? void 0 : value.name) === 'string'
        && typeof (value === null || value === void 0 ? void 0 : value.type) === 'string'
        && Object.values(Team).includes(value === null || value === void 0 ? void 0 : value.team)
        && typeof (value === null || value === void 0 ? void 0 : value.element) === 'string' && (0, elements_1.isElement)(value === null || value === void 0 ? void 0 : value.element)
        && (0, stats_1.isSavedStats)(value === null || value === void 0 ? void 0 : value.stats)
        && typeof (value === null || value === void 0 ? void 0 : value.init) === 'number'
        && typeof (value === null || value === void 0 ? void 0 : value.health) === 'number'
        && typeof (value === null || value === void 0 ? void 0 : value.mana) === 'number'
        && Array.isArray(value === null || value === void 0 ? void 0 : value.actions)
        && (value === null || value === void 0 ? void 0 : value.actions.every(function (action) { return typeof action === 'string' && (0, action_2.isActionId)(action); }))
        && (typeof (value === null || value === void 0 ? void 0 : value.lastTurn) === 'undefined' || typeof (value === null || value === void 0 ? void 0 : value.lastTurn) === 'number'));
}
exports.isSavedCharacter = isSavedCharacter;
var Character = (function () {
    function Character(name, type, team, element, actions, level, stats, affinity, lastTurn) {
        if (actions === void 0) { actions = ['hit']; }
        if (level === void 0) { level = 1; }
        if (lastTurn === void 0) { lastTurn = Number.POSITIVE_INFINITY; }
        this.id = (0, uuid_1.v4)();
        this.name = name;
        this.type = type;
        this.team = team;
        this.element = element;
        this.stats = new stats_1.Stats(stats, affinity, level);
        this.init = this.stats.init;
        this.health = this.stats.hp;
        this.mana = this.stats.mp;
        this.actions = actions.map(function (action) { return action_1.Action.load(action); });
        this.lastTurn = lastTurn;
    }
    Character.prototype.step = function () {
        this.lastTurn = 0;
        this.stats.step();
    };
    Character.prototype.incrementLastTurn = function () {
        this.lastTurn += 1;
    };
    Character.prototype.canUse = function (action) {
        return this.mana - action.mana >= 0;
    };
    Character.prototype.isAlive = function () {
        return this.health > 0;
    };
    Character.copy = function (character) {
        var stats = stats_1.Stats.save(character.stats);
        var copy = new Character(character.name, character.type, character.team, character.element, character.actions.map(function (action) { return action.id; }), stats.level, stats.initial, stats.affinity, character.lastTurn);
        copy.init = character.init;
        copy.health = character.health;
        copy.mana = character.mana;
        return copy;
    };
    Character.save = function (character) {
        return {
            id: character.id,
            name: character.name,
            type: character.type,
            team: character.team,
            element: character.element,
            stats: stats_1.Stats.save(character.stats),
            init: character.init,
            health: character.health,
            mana: character.mana,
            actions: character.actions.map(function (action) { return action_1.Action.save(action); }),
            lastTurn: (character.lastTurn === Number.POSITIVE_INFINITY) ? undefined : character.lastTurn,
        };
    };
    Character.load = function (savedCharacter) {
        if (!isSavedCharacter(savedCharacter)) {
            throw new Error('Not a valid character');
        }
        var character = new Character(savedCharacter.name, savedCharacter.type, savedCharacter.team, savedCharacter.element, savedCharacter.actions, savedCharacter.stats.level, savedCharacter.stats.initial, savedCharacter.stats.affinity, savedCharacter.lastTurn);
        character.id = savedCharacter.id;
        character.init = savedCharacter.init;
        character.health = savedCharacter.health;
        character.mana = savedCharacter.mana;
        return character;
    };
    Character.prototype.apply = function (action, target) {
        var _this = this;
        var events = action.effects.map(function (effect) {
            return effect.apply(target, _this);
        });
        if (events.some(function (event) { return event.type === 'damage'; })
            && !target.isAlive()) {
            target.init = target.stats.init;
            target.lastTurn = Number.POSITIVE_INFINITY;
            events.push(event_1.Events.characterDied(target));
        }
        return events;
    };
    Character.prototype.canTarget = function (action, target) {
        if (!(action.possibleTarget === action_1.Target.everyone)
            && ((action.possibleTarget === action_1.Target.allies && target.team !== this.team)
                || (action.possibleTarget === action_1.Target.enemies && target.team === this.team))) {
            return false;
        }
        return action.validateTargetStatus(target.isAlive());
    };
    Object.defineProperty(Character.prototype, "maxHealth", {
        get: function () {
            return this.stats.hp;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Character.prototype, "maxMana", {
        get: function () {
            return this.stats.mp;
        },
        enumerable: false,
        configurable: true
    });
    return Character;
}());
exports.Character = Character;
