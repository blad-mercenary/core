"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.defaultEventLogger = exports.EventLogger = exports.isSavedEventLogger = exports.Events = exports.Event = exports.isSavedEvent = exports.EventType = void 0;
var action_1 = require("./action");
var character_1 = require("./character");
var utils_1 = require("@blad-mercenary/utils");
var uuid_1 = require("uuid");
exports.EventType = [
    'actionUsed',
    'characterDied',
    'victory',
    'playerSave',
    'playerLoad',
    'damage',
    'heal',
    'resurrect',
    'noAction'
];
function isSavedEvent(value) {
    return (typeof value === 'object'
        && typeof (value === null || value === void 0 ? void 0 : value.type) === 'string'
        && exports.EventType.includes(value.type)
        && typeof (value === null || value === void 0 ? void 0 : value.timestamp) === 'string'
        && typeof (value === null || value === void 0 ? void 0 : value.details) === 'object');
}
exports.isSavedEvent = isSavedEvent;
var Event = (function () {
    function Event(type, details, timestamp) {
        this.type = type;
        if (timestamp) {
            this.timestamp = new Date(timestamp);
        }
        else {
            this.timestamp = new Date();
        }
        this.details = details;
    }
    Event.save = function (event) {
        return {
            type: event.type,
            timestamp: event.timestamp.toISOString(),
            details: event.details
        };
    };
    Event.load = function (savedEvent) {
        return new Event(savedEvent.type, savedEvent.details, savedEvent.timestamp);
    };
    return Event;
}());
exports.Event = Event;
exports.Events = {
    actionUsed: function (from, action, targets, timestamp) {
        if (timestamp === void 0) { timestamp = new Date(); }
        return new Event('actionUsed', { from: from.id, action: action_1.Action.save(action), targetsIds: targets.map(function (t) { return t.id; }) }, timestamp.toISOString());
    },
    characterDied: function (character, killedBy, timestamp) {
        if (timestamp === void 0) { timestamp = new Date(); }
        return new Event('characterDied', { characterId: character.id, killedBy: killedBy === null || killedBy === void 0 ? void 0 : killedBy.id }, timestamp.toISOString());
    },
    victory: function (fight, timestamp) {
        if (timestamp === void 0) { timestamp = new Date(); }
        return new Event('victory', { fight: fight.id, winner: fight.winner, fighters: fight.fighters.map(character_1.Character.save) }, timestamp.toISOString());
    },
    playerSave: function (player, timestamp) {
        if (timestamp === void 0) { timestamp = new Date(); }
        return new Event('playerSave', { player: player.id }, timestamp.toISOString());
    },
    playerLoad: function (player, timestamp) {
        if (timestamp === void 0) { timestamp = new Date(); }
        return new Event('playerLoad', { player: player.id }, timestamp.toISOString());
    },
    damage: function (from, target, damage, timestamp) {
        if (timestamp === void 0) { timestamp = new Date(); }
        return new Event('damage', { from: from.id, target: target.id, damage: damage }, timestamp.toISOString());
    },
    heal: function (from, target, heal, timestamp) {
        if (timestamp === void 0) { timestamp = new Date(); }
        return new Event('heal', { from: from.id, target: target.id, heal: heal }, timestamp.toISOString());
    },
    resurrect: function (from, target, timestamp) {
        if (timestamp === void 0) { timestamp = new Date(); }
        return new Event('resurrect', { from: from.id, target: target.id }, timestamp.toISOString());
    },
    noAction: function (from, timestamp) {
        if (timestamp === void 0) { timestamp = new Date(); }
        return new Event('noAction', { from: from.id }, timestamp.toISOString());
    }
};
function isSavedEventLogger(value) {
    return (typeof value === 'object'
        && typeof (value === null || value === void 0 ? void 0 : value.id) === 'string'
        && Array.isArray(value === null || value === void 0 ? void 0 : value.logs)
        && value.logs.every(function (el) { return isSavedEvent(el); }));
}
exports.isSavedEventLogger = isSavedEventLogger;
function printEvent(event, fighters) {
    function name(id) {
        var fighter = fighters.find(function (fighter) { return fighter.id === id; });
        if (!fighter) {
            throw new Error("Cannot find id ".concat(id));
        }
        return "".concat(fighter.name, " (").concat(fighter.id, ") (").concat(fighter.team, ")");
    }
    if (event.type === 'actionUsed') {
        var details = event.details;
        var from = name(details.from);
        var action = action_1.Action.load(details.action);
        var targets = details.targetsIds.map(name);
        return "".concat(from, " used ").concat(action.name, " on ").concat(targets.join(', '));
    }
    if (event.type === 'characterDied') {
        var details = event.details;
        var character = details.characterId;
        return "".concat(character, " died");
    }
    if (event.type === 'victory') {
        var details = event.details;
        return "".concat(details.winner, " won");
    }
    if (event.type === 'playerSave') {
        return 'Player save';
    }
    if (event.type === 'playerLoad') {
        return 'Player load';
    }
    if (event.type === 'damage') {
        var details = event.details;
        var from = name(details.from);
        var target = name(details.target);
        return "".concat(from, " inflicted ").concat(details.damage, " damage to ").concat(target);
    }
    if (event.type === 'heal') {
        var details = event.details;
        var from = name(details.from);
        var target = name(details.target);
        return "".concat(from, " healed ").concat(target, " of ").concat(details.heal, " damage");
    }
    if (event.type === 'resurrect') {
        var details = event.details;
        var from = name(details.from);
        var target = name(details.target);
        return "".concat(from, " resurected ").concat(target);
    }
    if (event.type === 'noAction') {
        var details = event.details;
        var from = name(details.from);
        return "".concat(from, " did nothing");
    }
    throw new Error("Unknown error type ".concat(event.type));
}
var EventLogger = (function () {
    function EventLogger(id, logs) {
        if (id === void 0) { id = (0, uuid_1.v4)(); }
        this.id = id;
        if (logs) {
            this.logs = logs;
            this.lastRead = logs.length;
        }
        else {
            this.logs = [];
            this.lastRead = 0;
        }
    }
    EventLogger.prototype.log = function (event) {
        var _a;
        utils_1.logger.info('event', event);
        if (Array.isArray(event)) {
            (_a = this.logs).push.apply(_a, event);
        }
        else {
            this.logs.push(event);
        }
    };
    EventLogger.prototype.getEvents = function (since) {
        if (since) {
            return this.logs.filter(function (event) { return event.timestamp > since; });
        }
        else {
            return this.logs;
        }
    };
    EventLogger.prototype.getLatest = function () {
        var latest = this.logs.slice(this.lastRead);
        this.lastRead = this.logs.length;
        return latest;
    };
    EventLogger.save = function (eventLogger) {
        return {
            id: eventLogger.id,
            logs: eventLogger.logs.map(Event.save)
        };
    };
    EventLogger.load = function (sel) {
        return new EventLogger(sel.id, sel.logs.map(Event.load));
    };
    EventLogger.print = function (events, fighters) {
        return events.map(function (event) { return "".concat(event.timestamp, " - ").concat(printEvent(event, fighters)); });
    };
    return EventLogger;
}());
exports.EventLogger = EventLogger;
exports.defaultEventLogger = new EventLogger('default');
