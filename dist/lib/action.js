"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Action = exports.TargetGroupIs = exports.SelectDeadTargetIs = exports.Target = void 0;
var action_1 = require("../data/action");
var Target;
(function (Target) {
    Target[Target["everyone"] = 0] = "everyone";
    Target[Target["allies"] = 1] = "allies";
    Target[Target["enemies"] = 2] = "enemies";
    Target[Target["self"] = 3] = "self";
})(Target = exports.Target || (exports.Target = {}));
var SelectDeadTargetIs;
(function (SelectDeadTargetIs) {
    SelectDeadTargetIs[SelectDeadTargetIs["allowed"] = 0] = "allowed";
    SelectDeadTargetIs[SelectDeadTargetIs["disallowed"] = 1] = "disallowed";
    SelectDeadTargetIs[SelectDeadTargetIs["mandatory"] = 2] = "mandatory";
})(SelectDeadTargetIs = exports.SelectDeadTargetIs || (exports.SelectDeadTargetIs = {}));
var TargetGroupIs;
(function (TargetGroupIs) {
    TargetGroupIs[TargetGroupIs["impossible"] = 0] = "impossible";
    TargetGroupIs[TargetGroupIs["mandatory"] = 1] = "mandatory";
})(TargetGroupIs = exports.TargetGroupIs || (exports.TargetGroupIs = {}));
var Action = (function () {
    function Action(id, name, time, mana, effects, possibleTarget, groupAction, deathSelection) {
        this.id = id;
        this.name = name;
        this.time = time;
        this.mana = mana;
        this.effects = effects;
        this.possibleTarget = possibleTarget;
        this.groupAction = groupAction;
        this.deathSelection = deathSelection;
    }
    Action.save = function (action) {
        if (!(0, action_1.isActionId)(action.id)) {
            throw new Error("Invalid action ".concat(action));
        }
        return action.id;
    };
    Action.load = function (actionId) {
        if (!(0, action_1.isActionId)(actionId)) {
            throw new Error("Trying to load invalid Action data ".concat(JSON.stringify(actionId)));
        }
        return action_1.Actions[actionId]();
    };
    Action.prototype.validateTargetStatus = function (isTargetAlive) {
        if (this.deathSelection === SelectDeadTargetIs.allowed) {
            return true;
        }
        else if (this.deathSelection === SelectDeadTargetIs.disallowed) {
            return isTargetAlive;
        }
        else if (this.deathSelection === SelectDeadTargetIs.mandatory) {
            return !isTargetAlive;
        }
        else {
            throw new Error("Invalid deathSelection ".concat(this.deathSelection));
        }
    };
    return Action;
}());
exports.Action = Action;
