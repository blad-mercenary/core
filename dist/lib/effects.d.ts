import { EffectDetails, EffectType, EffectImplementation } from '../data/effect';
import { Character } from './character';
import { Event } from './event';
export declare class Effect {
    type: EffectType;
    details: EffectDetails;
    private _apply;
    constructor(type: EffectType, details: EffectDetails, apply: EffectImplementation);
    apply(target: Character, from: Character): Event;
}
