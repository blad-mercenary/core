import { Character } from './character';
import { Action } from './action';
import { Fight } from './fight';
export interface Selection {
    action: Action;
    targets: Character[];
}
declare function isSelection(obj: any): obj is Selection;
declare function getSelectionsForAction(action: Action, character: Character, fight: Fight): Selection[];
declare function getSelectionsForGroupAction(action: Action, character: Character, fight: Fight): Selection[];
declare function getPossibleSelections(character: Character, fight: Fight): Selection[];
declare function validate(selection: Selection, character: Character, fight: Fight): boolean;
export declare const Selection: {
    isSelection: typeof isSelection;
    getSelectionsForAction: typeof getSelectionsForAction;
    getSelectionsForGroupAction: typeof getSelectionsForGroupAction;
    getPossibleSelections: typeof getPossibleSelections;
    validate: typeof validate;
};
export {};
