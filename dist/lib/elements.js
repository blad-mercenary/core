"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isElement = exports.elementalWeakness = exports.allElements = void 0;
exports.allElements = ['Wood', 'Fire', 'Earth', 'Metal', 'Water'];
exports.elementalWeakness = {
    Wood: 'Water',
    Fire: 'Wood',
    Earth: 'Fire',
    Metal: 'Earth',
    Water: 'Metal'
};
function isElement(string) {
    return exports.allElements.includes(string);
}
exports.isElement = isElement;
