import { Team, Character, SavedCharacter } from './character';
import { Selection } from './selection';
import { EventLogger, SavedEventLogger } from './event';
export interface SavedFight {
    fighters: SavedCharacter[];
    finished: boolean;
    winner?: Team;
    logger: SavedEventLogger;
}
export declare function isSavedFight(value: any): value is SavedFight;
export declare class Fight {
    readonly id: string;
    fighters: Character[];
    finished: boolean;
    winner?: Team;
    readonly eventLogger: EventLogger;
    constructor(fighters: Character[], eventLogger?: EventLogger);
    static save(fight: Fight): SavedFight;
    static load(savedFight: SavedFight): Fight;
    getActiveFighter(): Character;
    getNextActiveFighters(): Character[];
    validateSelection(activeFighter: Character, selection?: number | Selection): Selection | undefined;
    step(selection?: number | Selection): void;
    hasFighterAliveIn(team: Team): boolean;
    getTeam(team: Team): Character[];
    victoryBy(team: Team): void;
}
