import { ActionId } from '../data/action';
import { Effect } from './effects';
export declare enum Target {
    everyone = 0,
    allies = 1,
    enemies = 2,
    self = 3
}
export declare enum SelectDeadTargetIs {
    allowed = 0,
    disallowed = 1,
    mandatory = 2
}
export declare enum TargetGroupIs {
    impossible = 0,
    mandatory = 1
}
export declare class Action {
    id: ActionId;
    name: string;
    time: number;
    mana: number;
    effects: Effect[];
    possibleTarget: Target;
    groupAction: TargetGroupIs;
    deathSelection: SelectDeadTargetIs;
    constructor(id: ActionId, name: string, time: number, mana: number, effects: Effect[], possibleTarget: Target, groupAction: TargetGroupIs, deathSelection: SelectDeadTargetIs);
    static save(action: Action): ActionId;
    static load(actionId: string): Action;
    validateTargetStatus(isTargetAlive: boolean): boolean;
}
