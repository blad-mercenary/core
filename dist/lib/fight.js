"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Fight = exports.isSavedFight = void 0;
var character_1 = require("./character");
var selection_1 = require("./selection");
var event_1 = require("./event");
var uuid_1 = require("uuid");
var utils_1 = require("./utils");
var data_1 = require("../data");
function isSavedFight(value) {
    return (typeof value === 'object'
        && Object.prototype.hasOwnProperty.call(value, 'fighters') && Array.isArray(value.fighters)
        && value.fighters.every(function (f) { return (0, character_1.isSavedCharacter)(f); })
        && Object.prototype.hasOwnProperty.call(value, 'finished')
        && (value.winner === undefined
            || Object.keys(character_1.Team).includes(value.winner)));
}
exports.isSavedFight = isSavedFight;
var Fight = (function () {
    function Fight(fighters, eventLogger) {
        if (fighters.length === 0) {
            throw new Error('Invalid fight, no fighters given');
        }
        if (!fighters.some(function (c) { return c.team === character_1.Team.player; })) {
            throw new Error('Invalid fight, no fighters controlled by the player');
        }
        if (!fighters.some(function (c) { return c.team !== character_1.Team.player; })) {
            throw new Error('Invalid fight, no fighters not controlled by the player');
        }
        this.id = (0, uuid_1.v4)();
        this.fighters = fighters;
        this.finished = false;
        if (eventLogger) {
            this.eventLogger = eventLogger;
        }
        else {
            this.eventLogger = new event_1.EventLogger();
        }
    }
    Fight.save = function (fight) {
        return {
            fighters: fight.fighters.map(character_1.Character.save),
            finished: fight.finished,
            winner: fight.winner,
            logger: event_1.EventLogger.save(fight.eventLogger)
        };
    };
    Fight.load = function (savedFight) {
        var fight = new Fight(savedFight.fighters.map(character_1.Character.load), event_1.EventLogger.load(savedFight.logger));
        fight.finished = savedFight.finished;
        fight.winner = savedFight.winner;
        return fight;
    };
    Fight.prototype.getActiveFighter = function () {
        return this.getNextActiveFighters()[0];
    };
    Fight.prototype.getNextActiveFighters = function () {
        if (this.finished) {
            throw new Error('Fight already finished');
        }
        var activeFighters = this.fighters.filter(function (fighter) { return fighter.isAlive(); });
        if (activeFighters.length === 0) {
            throw new Error('No valid fighters');
        }
        activeFighters.sort(function (a, b) {
            if (a.init > b.init) {
                return 1;
            }
            if (b.init > a.init) {
                return -1;
            }
            if (a.lastTurn < b.lastTurn) {
                return 1;
            }
            if (b.lastTurn > a.lastTurn) {
                return -1;
            }
            if (a.id > b.id) {
                return 1;
            }
            if (b.id > a.id) {
                return -1;
            }
            return 0;
        });
        var activeFighter = activeFighters[0];
        if (activeFighter.init > 0) {
            var i_1 = activeFighter.init;
            this.fighters = this.fighters.map(function (fighter) {
                fighter.init = fighter.init - i_1;
                return fighter;
            });
        }
        return activeFighters;
    };
    Fight.prototype.validateSelection = function (activeFighter, selection) {
        var possibleActions = selection_1.Selection.getPossibleSelections(activeFighter, this);
        if (typeof selection === 'object') {
            var validated = selection_1.Selection.validate(selection, activeFighter, this);
            if (selection_1.Selection.isSelection(selection) && validated) {
                return selection;
            }
            throw new Error("Invalid selection ".concat(JSON.stringify(selection)));
        }
        if (typeof selection === 'number') {
            if (possibleActions[selection]) {
                return possibleActions[selection];
            }
            throw new Error("Invalid selection index ".concat(selection));
        }
        if (selection === undefined) {
            if (possibleActions.length === 0) {
                return;
            }
            var random = (0, utils_1.getRandomInt)(0, possibleActions.length - 1);
            return possibleActions[random];
        }
        throw new Error("Invalid selection ".concat(selection));
    };
    Fight.prototype.step = function (selection) {
        var _this = this;
        var activeFighter = this.getActiveFighter();
        this.fighters.forEach(function (fighter) {
            if (fighter.isAlive()) {
                fighter.incrementLastTurn();
            }
        });
        activeFighter.step();
        var selectedAction = this.validateSelection(activeFighter, selection)
            || { action: data_1.Actions.skip(), targets: [activeFighter] };
        var action = selectedAction.action, targets = selectedAction.targets;
        this.eventLogger.log(event_1.Events.actionUsed(activeFighter, action, targets));
        targets.forEach(function (target) {
            var events = activeFighter.apply(action, target);
            _this.eventLogger.log(events);
        });
        activeFighter.init += action.time;
        activeFighter.mana -= action.mana;
        if (!this.hasFighterAliveIn(character_1.Team.player)) {
            this.victoryBy(character_1.Team.enemy);
        }
        else if (!this.hasFighterAliveIn(character_1.Team.enemy)) {
            this.victoryBy(character_1.Team.player);
        }
    };
    Fight.prototype.hasFighterAliveIn = function (team) {
        return this.fighters.filter(function (c) { return c.team === team; }).some(function (c) { return c.isAlive(); });
    };
    Fight.prototype.getTeam = function (team) {
        return this.fighters.filter(function (fighter) { return fighter.team === team; });
    };
    Fight.prototype.victoryBy = function (team) {
        if (this.finished) {
            throw new Error('Fight already finished');
        }
        this.finished = true;
        this.winner = team;
        this.eventLogger.log(event_1.Events.victory(this));
    };
    return Fight;
}());
exports.Fight = Fight;
