export declare type Element = 'Wood' | 'Fire' | 'Earth' | 'Metal' | 'Water';
export declare const allElements: string[];
export declare const elementalWeakness: {
    [key in Element]: Element;
};
export declare function isElement(string: string): string is Element;
