import { Action } from './action';
import { ActionId } from '../data/action';
import { Character, Team, SavedCharacter } from './character';
import { Player } from './player';
import { Fight } from './fight';
export interface ActionUsedEventDetails {
    from: string;
    action: ActionId;
    targetsIds: string[];
}
export interface CharacterDiedEventDetails {
    characterId: string;
    killedBy?: string;
}
export interface VictoryEventDetails {
    fight: string;
    winner: Team;
    fighters: SavedCharacter[];
}
export interface PlayerSaveEventDetails {
    player: string;
}
export interface PlayerLoadEventDetails {
    player: string;
}
export interface DamageEventDetails {
    from: string;
    target: string;
    damage: number;
}
export interface HealEventDetails {
    from: string;
    target: string;
    heal: number;
}
export interface ResurrectEventDetails {
    from: string;
    target: string;
}
export interface NoActionEventDetails {
    from: string;
}
declare type EventsDetails = (ActionUsedEventDetails | CharacterDiedEventDetails | VictoryEventDetails | PlayerSaveEventDetails | PlayerLoadEventDetails | DamageEventDetails | HealEventDetails | ResurrectEventDetails | NoActionEventDetails);
export declare type EventType = 'actionUsed' | 'characterDied' | 'victory' | 'playerSave' | 'playerLoad' | 'damage' | 'heal' | 'resurrect' | 'noAction';
export declare const EventType: string[];
export interface SavedEvent {
    type: EventType;
    timestamp: string;
    details: EventsDetails;
}
export declare function isSavedEvent(value: any): value is SavedEvent;
export declare class Event {
    readonly type: EventType;
    readonly timestamp: Date;
    readonly details: EventsDetails;
    constructor(type: EventType, details: EventsDetails, timestamp?: string);
    static save(event: Event): SavedEvent;
    static load(savedEvent: SavedEvent): Event;
}
export declare const Events: {
    actionUsed: (from: Character, action: Action, targets: Character[], timestamp?: Date) => Event;
    characterDied: (character: Character, killedBy?: Character | undefined, timestamp?: Date) => Event;
    victory: (fight: Fight, timestamp?: Date) => Event;
    playerSave: (player: Player, timestamp?: Date) => Event;
    playerLoad: (player: Player, timestamp?: Date) => Event;
    damage: (from: Character, target: Character, damage: number, timestamp?: Date) => Event;
    heal: (from: Character, target: Character, heal: number, timestamp?: Date) => Event;
    resurrect: (from: Character, target: Character, timestamp?: Date) => Event;
    noAction: (from: Character, timestamp?: Date) => Event;
};
export interface SavedEventLogger {
    id: string;
    logs: SavedEvent[];
}
export declare function isSavedEventLogger(value: any): value is SavedEventLogger;
export declare class EventLogger {
    id: string;
    private logs;
    private lastRead;
    constructor(id?: string, logs?: Event[]);
    log(event: Event | Event[]): void;
    getEvents(since?: Date): Event[];
    getLatest(): Event[];
    static save(eventLogger: EventLogger): SavedEventLogger;
    static load(sel: SavedEventLogger): EventLogger;
    static print(events: Event[], fighters: Character[]): string[];
}
export declare const defaultEventLogger: EventLogger;
export {};
