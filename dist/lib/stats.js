"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Stats = exports.StatModifier = exports.isSavedStats = exports.isSavedModifier = exports.statTypes = exports.toFullStatsSet = exports.isFullStatsSet = void 0;
function isFullStatsSet(saved) {
    return typeof saved === 'object'
        && typeof (saved === null || saved === void 0 ? void 0 : saved.hp) === 'number'
        && typeof (saved === null || saved === void 0 ? void 0 : saved.mp) === 'number'
        && typeof (saved === null || saved === void 0 ? void 0 : saved.atk) === 'number'
        && typeof (saved === null || saved === void 0 ? void 0 : saved.def) === 'number'
        && typeof (saved === null || saved === void 0 ? void 0 : saved.speAtk) === 'number'
        && typeof (saved === null || saved === void 0 ? void 0 : saved.speDef) === 'number'
        && typeof (saved === null || saved === void 0 ? void 0 : saved.init) === 'number';
}
exports.isFullStatsSet = isFullStatsSet;
function getStatSet(value) {
    return { hp: value, mp: value, atk: value, def: value, speAtk: value, speDef: value, init: value };
}
function toFullStatsSet(partial, defaultStat) {
    if (defaultStat === void 0) { defaultStat = 1; }
    return __assign(__assign({}, getStatSet(defaultStat)), partial);
}
exports.toFullStatsSet = toFullStatsSet;
exports.statTypes = ['hp', 'mp', 'atk', 'def', 'speAtk', 'speDef', 'init'];
function isSavedModifier(saved) {
    return typeof saved === 'object'
        && isFullStatsSet(saved === null || saved === void 0 ? void 0 : saved.value)
        && ((saved === null || saved === void 0 ? void 0 : saved.expires) === undefined || typeof saved.expires === 'number');
}
exports.isSavedModifier = isSavedModifier;
function isSavedStats(saved) {
    return typeof saved === 'object'
        && isFullStatsSet(saved === null || saved === void 0 ? void 0 : saved.initial)
        && isFullStatsSet(saved === null || saved === void 0 ? void 0 : saved.affinity)
        && Array.isArray(saved === null || saved === void 0 ? void 0 : saved.modifiers) && saved.modifiers.every(function (element) { return isSavedModifier(element); })
        && typeof (saved === null || saved === void 0 ? void 0 : saved.level) === 'number';
}
exports.isSavedStats = isSavedStats;
var StatModifier = (function () {
    function StatModifier(value, expires) {
        this.value = __assign(__assign({}, value), getStatSet(0));
        this.expires = expires;
    }
    return StatModifier;
}());
exports.StatModifier = StatModifier;
var Stats = (function () {
    function Stats(init, affinity, level, modifiers) {
        if (modifiers === void 0) { modifiers = []; }
        this.initial = toFullStatsSet(init);
        this.affinity = toFullStatsSet(affinity);
        this._modifiers = modifiers;
        this.level = level;
        this.statsCache = getStatSet(1);
        this.compute(level);
    }
    Stats.prototype.save = function () {
        return {
            initial: this.initial,
            affinity: this.affinity,
            level: this.level,
            modifiers: this._modifiers.map(function (modifier) { return ({
                value: modifier.value,
                expires: modifier.expires
            }); })
        };
    };
    Stats.save = function (stats) {
        return stats.save();
    };
    Stats.load = function (saved) {
        return new Stats(saved.initial, saved.affinity, saved.level, saved.modifiers);
    };
    Stats.prototype.compute = function (level) {
        var _this = this;
        if (level) {
            this.level = level;
        }
        var modifiers = this._modifiers.reduce(function (acc, cur) {
            var result = getStatSet(0);
            exports.statTypes.forEach(function (stat) {
                result[stat] = acc[stat] * cur.value[stat];
            });
            return result;
        }, getStatSet(1));
        exports.statTypes.forEach(function (stat) {
            _this.statsCache[stat] = Math.floor(_this.initial[stat] * _this.affinity[stat] * _this.level * modifiers[stat]);
        });
    };
    Object.defineProperty(Stats.prototype, "modifiers", {
        get: function () {
            return this._modifiers;
        },
        set: function (modifiers) {
            this._modifiers = modifiers;
            this.compute();
        },
        enumerable: false,
        configurable: true
    });
    Stats.prototype.step = function () {
        var modifierCount = this._modifiers.length;
        this._modifiers = this._modifiers.map(function (modifier) {
            if (modifier.expires !== undefined) {
                modifier.expires -= 1;
            }
            return modifier;
        }).filter(function (modifier) {
            return modifier.expires === undefined || modifier.expires >= 0;
        });
        if (this._modifiers.length !== modifierCount) {
            this.compute(this.level);
        }
    };
    Stats.prototype.get = function (stat) {
        return this.statsCache[stat];
    };
    Object.defineProperty(Stats.prototype, "hp", {
        get: function () {
            return this.get('hp');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Stats.prototype, "mp", {
        get: function () {
            return this.get('mp');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Stats.prototype, "atk", {
        get: function () {
            return this.get('atk');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Stats.prototype, "def", {
        get: function () {
            return this.get('def');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Stats.prototype, "speAtk", {
        get: function () {
            return this.get('speAtk');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Stats.prototype, "speDef", {
        get: function () {
            return this.get('speDef');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Stats.prototype, "init", {
        get: function () {
            return this.get('init');
        },
        enumerable: false,
        configurable: true
    });
    return Stats;
}());
exports.Stats = Stats;
