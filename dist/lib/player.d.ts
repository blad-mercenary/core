import { Character, SavedCharacter } from './character';
import { EventLogger, SavedEventLogger } from './event';
export interface SavedPlayer {
    id: string;
    name: string;
    gold: number;
    team: SavedCharacter[];
    eventLogger: SavedEventLogger;
    version: number;
}
export declare class Player {
    private _id;
    name: string;
    gold: number;
    team: Character[];
    eventLogger: EventLogger;
    version: number;
    constructor(id?: string, name?: string, gold?: number, team?: Character[], eventLogger?: EventLogger, version?: number);
    static save(player: Player, noLog?: boolean): SavedPlayer;
    static load(sp: SavedPlayer, version?: number, noLog?: boolean): Player;
    get id(): string;
    get status(): SavedPlayer;
}
export declare function isSavedPlayer(value: any): value is SavedPlayer;
