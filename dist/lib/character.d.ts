import { Action } from './action';
import { ActionId } from '../data/action';
import { Event } from './event';
import { Stats, StatsSet, SavedStats } from './stats';
import { Element } from './elements';
export declare enum Team {
    player = "player",
    enemy = "enemy"
}
export interface SavedCharacter {
    id: string;
    name: string;
    type: string;
    team: Team;
    element: Element;
    stats: SavedStats;
    init: number;
    health: number;
    mana: number;
    actions: ActionId[];
    lastTurn?: number;
}
export declare function isSavedCharacter(value: any): value is SavedCharacter;
export declare class Character {
    id: string;
    name: string;
    type: string;
    team: Team;
    element: Element;
    init: number;
    health: number;
    mana: number;
    actions: Action[];
    lastTurn: number;
    stats: Stats;
    constructor(name: string, type: string, team: Team, element: Element, actions: ("skip" | "hit" | "ice shard" | "ice wave" | "heal" | "raise")[] | undefined, level: number | undefined, stats: StatsSet, affinity: StatsSet, lastTurn?: number);
    step(): void;
    incrementLastTurn(): void;
    canUse(action: Action): boolean;
    isAlive(): boolean;
    static copy(character: Character): Character;
    static save(character: Character): SavedCharacter;
    static load(savedCharacter: SavedCharacter): Character;
    apply(action: Action, target: Character): Event[];
    canTarget(action: Action, target: Character): boolean;
    get maxHealth(): number;
    get maxMana(): number;
}
