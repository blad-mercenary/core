"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isSavedPlayer = exports.Player = void 0;
var uuid_1 = require("uuid");
var character_1 = require("./character");
var event_1 = require("./event");
var character_2 = require("../data/character");
var _1 = require(".");
var Player = (function () {
    function Player(id, name, gold, team, eventLogger, version) {
        if (id === void 0) { id = (0, uuid_1.v4)(); }
        if (name === void 0) { name = 'Jack'; }
        if (gold === void 0) { gold = 1000; }
        if (team === void 0) { team = [character_2.Characters.goblin(character_1.Team.player)]; }
        if (eventLogger === void 0) { eventLogger = new event_1.EventLogger(); }
        if (version === void 0) { version = 0; }
        this._id = id;
        this.name = name;
        this.gold = gold;
        this.team = team;
        this.eventLogger = eventLogger;
        this.version = version;
    }
    Player.save = function (player, noLog) {
        if (!noLog) {
            player.eventLogger.log(event_1.Events.playerSave(player));
        }
        return {
            id: player._id,
            name: player.name,
            gold: player.gold,
            team: player.team.map(character_1.Character.save),
            eventLogger: event_1.EventLogger.save(player.eventLogger),
            version: player.version
        };
    };
    Player.load = function (sp, version, noLog) {
        if (version === void 0) { version = 0; }
        var player = new Player(sp.id, sp.name, sp.gold, sp.team.map(character_1.Character.load), event_1.EventLogger.load(sp.eventLogger), version);
        if (!noLog) {
            player.eventLogger.log(event_1.Events.playerLoad(player));
        }
        return player;
    };
    Object.defineProperty(Player.prototype, "id", {
        get: function () {
            return this._id;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Player.prototype, "status", {
        get: function () {
            return Player.save(this, true);
        },
        enumerable: false,
        configurable: true
    });
    return Player;
}());
exports.Player = Player;
function isSavedPlayer(value) {
    return (typeof value === 'object'
        && typeof (value === null || value === void 0 ? void 0 : value.id) === 'string'
        && typeof (value === null || value === void 0 ? void 0 : value.name) === 'string'
        && typeof (value === null || value === void 0 ? void 0 : value.gold) === 'number'
        && Array.isArray(value === null || value === void 0 ? void 0 : value.team)
        && value.team.every(function (el) { return (0, character_1.isSavedCharacter)(el); })
        && typeof (0, _1.isSavedEventLogger)(value === null || value === void 0 ? void 0 : value.eventLogger)
        && typeof (value === null || value === void 0 ? void 0 : value.version) === 'number');
}
exports.isSavedPlayer = isSavedPlayer;
